$('#btndiario').on('click',function(){
	if($("#btndiario").val()=="ver"){
        var opc="2";
        $.ajax({
            url : 'gestfct.php',
            data : { "opc": opc },
            type : 'POST',
            dataType : 'html',
            success : function(data) {
                $data='<div class="panel panel-default"><div class="panel-body">'+data+'</div></div>';
                $("#diario").html(data);
                $('#btndiario').html("Ocultar diario");
                $("#btndiario").val("ocultar");
            },
         
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        });
    }
    if($("#btndiario").val()=="ocultar"){
        $("#diario").html("");
        $("#btndiario").val("ver");
        $('#btndiario').html("Ver diario");
    }
});

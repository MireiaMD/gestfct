$(function() {
    
    var tr;
    var fila;
    
    var tabla=$('#tabla').DataTable( {
        responsive: true,
        "language": {"url": "../ajax/LangSpanish.txt" },
        //para coger de bd
        "ajax": 'cargardata.php?opc=6',
        
        "columns": [
            { "data": "nombre" },
            { "data": "tutorempresa" },
            { "data": "telefono" },
            { "data": "movil" },
            { "data": "email" },
            { "data": "id","class":"hidden" },
            {data: null,
                orderable:false,
                searchable: false,
                sortable:false,
                defaultContent: "<div class='text-center'><button type='button' class='detalle btn btn-deta btn-xs' title='Detallar registro'><i class='fa fa-paperclip'></i></button>&nbsp;<button type='button' class='editar btn btn-xs btn-edit' title='Editar registro'><i class='fa fa-pencil-square-o'></i></button>&nbsp;<button type='button' class='anadirv btn btn-xs btn-anav' title='Añadir vacante'><i class='fa fa-plus'></i></button></div>" 
            }
        ],

        dom: "<'row'<'form-inline'<'col-sm-3'l>>><rt><'row'<'form-inline'<'col-sm-7'B><'col-sm-5'f>>><rt><'row'<'form-inline'<'col-sm-6'i><'col-sm-6'p>>>",
        
        buttons:[
            'excel', 'pdf',
            {
                text:'Añadir',
                titleAttr:'Añadir',
                className: 'btn btn-anadir',
                action: function(){ anadir(); }
            }
        ]
    });
    
    var comprobar=function(variable){
        var nulo=false;
        if(variable==""){
        nulo=true;
        }
        return nulo;
    }
    
    var llamarAnadir=function(){
        var opc ="1";
        $.ajax({
            url : 'gestemp.php',

            data : $('#miFormulario').serialize()+"&opc="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          

            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarEditar=function(){
        var opc="3";
        $.ajax({
            url : 'gestemp.php',

            data:$('#miFormulario').serialize()+"&opc="+opc,
            
            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          
            
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#btnSubmit').on('click',function(e){
        e.preventDefault();
        if($("#myModalLabel").html()=="<strong>Añadir Empresa</strong>"){
            
            valores = {
                "nombre": $('#nombre').val(),
                "tutorempresa": $('#tutorempresa').val(),
                "telefono":$('#telefono').val(),
                "movil":$('#movil').val(),
                "email":$('#email').val(),
                "id": $('#clave').val()
            };
            
            if(comprobar(valores.nombre) || comprobar(valores.tutorempresa) || comprobar(valores.telefono) || comprobar(valores.movil) || comprobar(valores.email)){
                swal({
                  title: "Error",
                  text: "Los campos: Nombre, tutor, telefono, movil y correo electronico no deben quedar vacios",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
            else{
                //llamar a funcion que llama al php
                llamarAnadir();
            }
        }
        else {
            tabla.draw(false);
            //llamar a funcion que llama al php
            llamarEditar();
        }
        
        $('#miVentana').modal('hide');

    });
    
    var anadir= function(){
        $('#miFormulario')[0].reset();

        $(".titulo").html("<strong>Añadir Empresa</strong>");
        $("#btnSubmit").html("<strong>Guarda Nuevo</strong>");
        $("#btnSubmit").addClass('btn-primary');
        
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});

    };

    var rellena=function(id,tipo){
        var opc="2";
        $('#miFormulario')[0].reset();
        $.ajax({
            url : 'gestemp.php',

            data : {"id": id,"tipo": tipo,"opc":opc},
            
            type : 'POST',

            dataType : 'json',

            success: function(data) {
                $('#nombre').val(data.nombre);
                $('#razon_social').val(data.razon_social);
                $('#movil').val(data.movil);
                $('#tutorempresa').val(data.tutorempresa);
                $('#pais').val(data.pais);
                $('#direccion').val(data.direccion);
                $('#codpostal').val(data.codpostal);
                $('#poblacion').val(data.poblacion);
                $('#provincia').val(data.provincia);
                $('#telefono').val(data.telefono);
                $('#email').val(data.email);
            },          
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#tabla tbody').on('click','.editar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();
       
        var nombre=tabla.cell(fila,0).data();
        $(".titulo").html("<strong>Editar Empresa "+nombre+"</strong>");
        $("#btnSubmit").html("<strong>Guarda Cambios</strong>");
        $("#btnSubmit").addClass('btn-primary');
        
        var id=tabla.cell(fila,5).data();

        rellena(id);       

        var valores=[
            $('#nombre').val(tabla.cell(fila,0).data()),
            $('#tutorempresa').val(tabla.cell(fila,1).data()),
            $('#telefono').val(tabla.cell(fila,2).data()),
            $('#movil').val(tabla.cell(fila,3).data()),
            $('#email').val(tabla.cell(fila,4).data()),
            $('#clave').val(id),
        ];
        
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});
    });

    var llamarAnadirVac=function(){
        var opc="4";
        $.ajax({
            url : 'gestemp.php',

            data : $('#formVacante').serialize()+"&opc="+opc,

            type : 'POST',

            success: function(data) {
               swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
               tabla.ajax.reload();
            },          

            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#tabla tbody').on('click','.anadirv',function(){
        $('#formVacante')[0].reset();
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();
        
        var idempresa=tabla.cell(fila,5).data();
        $('#idempresa').val(idempresa);
        
        $('#ventanaAnadirvac').modal({show:true, backdrop:false, keyboard:false});
    });

    $('#guardar').on('click',function(e){
        e.preventDefault();
            
        valores = {
            "curso_escolar": $('#curso_escolar').val(),
            "requisitostec": $('#requisitostec').val()
        };
        
        if(comprobar(valores.curso_escolar) || comprobar(valores.requisitostec)){
            swal({
                  title: "Error",
                  text: "Alguno de los campos está vacio",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });            
        }
        else{
             llamarAnadirVac();
            $('#ventanaAnadirvac').modal('hide');
        }

    });

    var detalles=function (clave){
        var opc="5";
        $.ajax({
            url : 'gestemp.php',
         
            data : { "id": clave, "opc":opc},
         
            type : 'POST',
         
            dataType : 'html',
         
            success : function(data) {
                $("#tablaempresa").html(data);
            },
         
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        });
    }

    $('#tabla tbody').on('click','.detalle',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        var nombre=tabla.cell(fila,0).data();
        var clave= tabla.cell(fila,5).data();
        detalles(clave);
        $(".titulo").html('<strong>Detalle de la empresa '+nombre+'</strong>');
        $('#ventanaDetalle').modal({show:true, backdrop:false, keyboard:false});
    });
    $('#curso_escolar').datepicker({
        format: "yyyy",
        startView: 2,
        minViewMode: 2,
        maxViewMode: 2,
        language: "es",
        autoclose: true,
        assumeNearbyYear: 20,
        daysOfWeekDisabled: "0",
        startDate: '+0y'
    });
});
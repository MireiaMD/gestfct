var usuact=function (){
	$.ajax({
		type:"POST",
		url: "usuarios.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var usuinact=function (){
	$.ajax({
		type:"POST",
		url: "usu_inactivos.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var tutores=function (){
	$.ajax({
		type:"POST",
		url: "tutores.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var profesores=function (){
	$.ajax({
		type:"POST",
		url: "profesores.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var alumnos=function (){
	$.ajax({
		type:"POST",
		url: "alumnos.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var empresas=function (){
	$.ajax({
		type:"POST",
		url: "empresas.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var fcts=function (){
	$.ajax({
		type:"POST",
		url: "fcts.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var cerrar=function (){
	$.ajax({
		type:"POST",
		url: "cerrar.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

var perfil=function (){
	$.ajax({
		type:"POST",
		url: "perfil.php",
		success: function(data){
			$('#cargar').html(data);
		}
	});
}

function mueveReloj(){ 
   	momentoActual = new Date() 
   	hora = momentoActual.getHours() 
   	minuto = momentoActual.getMinutes() 
   	segundo = momentoActual.getSeconds() 
   	str_segundo = new String (segundo) 
   	if (str_segundo.length == 1) 
      	segundo = "0" + segundo 

   	str_minuto = new String (minuto) 
   	if (str_minuto.length == 1) 
      	minuto = "0" + minuto 

   	str_hora = new String (hora) 
   	if (str_hora.length == 1) 
      	hora = "0" + hora 
   	
   	horaImprimible = hora + " : " + minuto + " : " + segundo 

   	document.form_reloj.reloj.value = horaImprimible 
   	
   	setTimeout("mueveReloj()",1000) 
}

function show5(){
if (!document.layers&&!document.all&&!document.getElementById)
	return

var fecha_hora=new Date();
var hora=fecha_hora.getHours();
var minutos=fecha_hora.getMinutes();
var segundos=fecha_hora.getSeconds();

var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");

var dialetra=diasSemana[fecha_hora.getDay()];
var dia=fecha_hora.getDate();
var mesletra=meses[fecha_hora.getMonth()];
var ano=fecha_hora.getFullYear();

 if (minutos<=9)
 minutos="0"+minutos;
 if (segundos<=9)
 segundos="0"+segundos;

myclock="<font size='14' face='Arial'><b><center><font size='6'>"+dialetra+", "+dia+" de "+mesletra+" de "+ano+"</font></br>"+hora+":"+minutos+":"
 +segundos+"</center></b></font>";

if (document.layers){
	document.layers.liveclock.document.write(myclock);
	document.layers.liveclock.document.close();
}
else if (document.all)
	liveclock.innerHTML=myclock;
else if (document.getElementById)
	document.getElementById("liveclock").innerHTML=myclock;

setTimeout("show5()",1000)
}


        window.onload=show5
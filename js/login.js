$(function(){
	var comprobar=function(variable){
		var nulo=false;
		if(variable==""){
			nulo=true;
		}
		return nulo;
	};
	$('#enviar').on('click',function(e){
		if((comprobar($('#usuario').val()) || comprobar($('#passwd').val()))){
			e.preventDefault();
			$('#alerta').removeClass('hide');
			return false;
		};
	});
});
var modificar=function (opcion){
    $.ajax({
        url : 'modificar.php',
        data : { "op": opcion },
        type : 'POST',
        dataType : 'html',
        success : function(data) {
            $("#ventModif").html(data);
        },
     
        error : function(xhr, status) {
            swal({
              title: "Error",
              text: "Disculpe, existió un problema",
              type: "error",
              confirmButtonText: "Cerrar"
            });
        }
    });
}

var guardar=function (opcion){
    $.ajax({
        url : 'modificar.php',
     
        data:$('#miFormulario').serialize(),
        
        type : 'POST',
     
        dataType : 'text',
     
        success : function(data) {
            swal({
              title: "",
              text: data,
              type: "info",
              confirmButtonText: "Cerrar"
            });
        },
     
        error : function(xhr, status) {
            swal({
              title: "Error",
              text: "Disculpe, existió un problema",
              type: "error",
              confirmButtonText: "Cerrar"
            });
        }
    });
}

$('#datos').on('click',function(){
    var opc="datosp";
    modificar(opc);
    $('#ventModif').modal({show:true, backdrop:false,keyboard:false});

});

$('#passwd').on('click',function(){
    var opc="contra";
    modificar(opc);
    $('#ventModif').modal({show:true, backdrop:false, keyboard:false});
});

$('#ventModif').on('click','#guardar',function(e){
    e.preventDefault();
    if($("#myModalLabel").html()=="Modificacion datos personales"){ 
        var op2="gdatosp";
        guardar(op2);
    }
    else if($("#myModalLabel").html()=="Modificacion contraseña"){        
        var op2="gcontra";
        guardar(op2);
    }
      
    $('#ventModif').modal('hide');

});


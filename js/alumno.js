$(function() {  
    var tr;
    var fila;
    
    var tabla=$('#tabla').DataTable( {
        responsive: true,
        "language": {"url": "../ajax/LangSpanish.txt" },
        //para coger de bd
        "ajax": 'cargardata.php?opc=5',
        
        "columns": [
            { "data": "nombre" },
            { "data": "apellidos" },
            { "data": "dni" },
            { "data": "fechanac" },
            { "data": "titulo" },
            { "data": "id","class":"hidden" },
            { "data": "idtutor","class":"hidden" },
            {data: null,
                orderable:false,
                searchable: false,
                sortable:false,
                defaultContent: "<div class='text-center'><button type='button' class='detalle btn btn-deta btn-xs' title='Detallar registro'><i class='fa fa-paperclip'></i></button>&nbsp;<button type='button' class='editar btn btn-xs btn-edit' title='Editar registro'><i class='fa fa-pencil-square-o'></i></button>&nbsp;<button type='button' class='borrar btn btn-danger btn-xs' title='Borrar registro'><i class='fa fa-trash-o'></i></button></div>"
            }
        ],

        dom: "<'row'<'form-inline'<'col-sm-3'l>>><rt><'row'<'form-inline'<'col-sm-7'B><'col-sm-5'f>>><rt><'row'<'form-inline'<'col-sm-6'i><'col-sm-6'p>>>",
        
        buttons:[
            'excel', 'pdf',
            {
                text:'Añadir',
                titleAttr:'Añadir',
                className: 'btn btn-anadir',
                action: function(){ anadir(); }
            },
            {
                text:'Importar CSV',
                titleAttr:'Importar CSV',
                className: 'btn btn-csv',
                action: function(){ importar(); }
            }
        ]
    });


    var comprobar=function(variable){
        var nulo=false;
        if(variable==""){
        nulo=true;
        }
        return nulo;
    }

    var menorDiez=function(variable){
        if (parseInt(variable)<10){
            return "0"+variable;
        }
        else 
            return variable;
    }

    function nif(dni) {
      var numero;
      var letr;
      var letra;
      var expresion_regular_dni;
     
      expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
     
      if(expresion_regular_dni.test (dni) == true){
         numero = dni.substr(0,dni.length-1);
         letr = dni.substr(dni.length-1,1);
         numero = numero % 23;
         letra='TRWAGMYFPDXBNJZSQVHLCKET';
         letra=letra.substring(numero,numero+1);
        if (letra!=letr.toUpperCase()) {
           return 0;
         }
         else{
           return 1;
         }
      }
      else{
         return 2;
      }
    }

    var generafecha=function(){
        var f=new Date();
        var ano=f.getFullYear();
        var mes=f.getMonth()+1;
        var dia=f.getDate();

        var fecha=("0"+dia).slice(-2)+"/"+menorDiez(mes)+"/"+ano;

        return fecha;
    }

    var llamarAnadir=function(){
        var opc="1";
        $.ajax({
            url : 'gestalu.php',

            data : $('#miFormulario').serialize()+"&opc="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          

            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarEditar=function(){
        var opc="3";
        $.ajax({
            url : 'gestalu.php',

            data:$('#miFormulario').serialize()+"&opc="+opc,
            
            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          
            
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarBorrar=function(){
        var opc="4";
        $.ajax({
            url : 'gestalu.php',

            data : $('#miFormulario').serialize()+"&opc="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          
           
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#btnSubmit').on('click',function(e){
        e.preventDefault();
        if($("#myModalLabel").html()=="<strong>Añadir Alumno</strong>"){
            
            valores = {
                "nombre": $('#nombre').val(),
                "apellidos": $('#apellidos').val(),
                "dni": $('#dni').val(),
                "fechanac":$('#fechanac').val(),
                "titulo":$('#titulo').val(),
                "id":$('#clave').val(),
                "idtutor":$('#tutorA').val(),
                "email":$('#email').val(),
                "telefono":$('#telefono').val()
            };             

            var checkNif=nif($('#dni').val());

            if(comprobar(valores.email)||comprobar(valores.nombre)||comprobar(valores.apellidos)||comprobar(valores.telefono)||comprobar(valores.dni)){
                swal({
                  title: "Error",
                  text: "Los campos: Nombre, Apellidos, Correo electronico, Telefono y DNI no pueden quedar vacios",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
            else if(checkNif==0){
                swal({
                  title: "Error",
                  text: "Dni erroneo, la letra del NIF no se corresponde",
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
            }
            else if(checkNif==2){
                swal({
                  title: "Error",
                  text: "Dni erroneo, formato no válido",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
            else{
                //llamar a funcion que llama al php
                llamarAnadir();
            }
        }
        else if($("#myModalLabel").html()=="<strong>Borrar Alumno</strong>"){
            llamarBorrar();
        }
        else {
            tabla.draw(false);
            //llamar a funcion que llama al php
            llamarEditar();
        }
        
        $('#miVentana').modal('hide');

    });
    
    var importar= function(){
        $('#ventanaImportar').modal({show:true, backdrop:false, keyboard:false});

    };
    $("#subir").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("subir"));
        $.ajax({
            url: "gestalu.php",
            type: "post",
            dataType: "text",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
            .done(function(res){
                $("#mensaje").html("Respuesta: " + res);
            });
    });

    var anadir= function(){
        $('#miFormulario')[0].reset();
        $(".titulo").html("<strong>Añadir Alumno</strong>");
        $("#btnSubmit").html("<strong>Guarda Nuevo</strong>");
        $("#altaeditar").removeClass('hidden');
        $("#grupomail").removeClass('hidden');
        $('#grupobaja').addClass('hidden');
        $('#alumnoe').addClass('hidden');
        $('#grupoalta').removeClass('hidden');
        $("#btnSubmit").addClass('btn-primary');
        $("#btnSubmit").removeClass('btn-danger');
        $("#grupotutorA").removeClass('hidden');
        $("#grupotutorB").addClass('hidden');

        var alta=generafecha(); 
       
        $('#alta').val(alta);
        
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});

    };

    var rellena=function(id){
        var opc="2";
        $('#miFormulario')[0].reset();
        $.ajax({
            url : 'gestalu.php',

            data : {"id": id,"opc": opc},
            
            type : 'POST',

            dataType : 'json',

            success: function(data) {
                
                $('#nombre').val(data.nombre);
                $('#apellidos').val(data.apellidos);
                $('#dni').val(data.dni);
                $('#fechanac').val(data.fechanac);
                $('#titulo').val(data.titulo);
                $('#direccion').val(data.direccion);
                $('#codpostal').val(data.codpostal);
                $('#poblacion').val(data.poblacion);
                $('#provincia').val(data.provincia);
                $('#telefono').val(data.telefono);
                $('#estudiosant').val(data.estudiosant);
                $('#experiencia').val(data.experiencia);
                $('#preferencias').val(data.preferencias);
                $('#tutor').val(data.tutor);
                $('#email').val(data.email);
                
            },          
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#tabla tbody').on('click','.editar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();
        
        var id=tabla.cell(fila,5).data();
        rellena(id);

        $("#grupomail").removeClass('hidden');
        var nombre=tabla.cell(fila,0).data();
        var apellidos=tabla.cell(fila,1).data();
        $(".titulo").html("<strong>Editar alumno "+nombre+" "+apellidos+"</strong>");
        $("#btnSubmit").html("<strong>Guarda Cambios</strong>");
        $("#altaeditar").removeClass('hidden');
        $('#grupobaja').addClass('hidden');
        $('#grupoalta').addClass('hidden');     
        $("#btnSubmit").addClass('btn-primary');
        $("#btnSubmit").removeClass('btn-danger');
        
        
        var tutor=tabla.cell(fila,6).data();
        if(tutor==0){
            $("#grupotutorA").removeClass('hidden');
            $("#grupotutorB").addClass('hidden');
        }
        else{
            $("#grupotutorA").addClass('hidden');
            inputtut='<label class="col-xs-4" for="tutor">Tutor</label><input type="text" class="col-xs-7" name="tutor" id="tutor" placeholder="Tutor" disabled>';
            $('#grupotutorB').html(inputtut);
            $("#grupotutorB").removeClass('hidden');   
        }

        $('#clave').val(tabla.cell(fila,5).data());
        $('#alumnoe').removeClass('hidden');
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});

    });


    $('#tabla tbody').on('click','.borrar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        $(".titulo").html("<strong>Borrar Alumno</strong>");
        $("#btnSubmit").html("<strong>Borrar registro</strong>");
        $("#btnSubmit").attr("name","borrar");
        $('#grupobaja').removeClass('hidden');
        $('#grupoalta').addClass('hidden');
        $('#alumnoe').addClass('hidden');
        $("#btnSubmit").removeClass('btn-primary');
        $("#btnSubmit").addClass('btn-danger');
        $("#grupomail").addClass('hidden');
        $("#altaeditar").addClass('hidden');
        
        
        var baja=generafecha();
        var valores=[
        $('#nombre').val(tabla.cell(fila,0).data()),
        $('#apellidos').val(tabla.cell(fila,1).data()),
        $('#dni').val(tabla.cell(fila,2).data()),
        $('#baja').val(baja)
        ];
        $('#clave').val(tabla.cell(fila,5).data());       
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});
    });

    var detalles=function (clave){
        var opc="5";
        $.ajax({
            url : 'gestalu.php',
         
            data : { "id": clave,"opc":opc},
         
            type : 'POST',
         
            dataType : 'html',
         
            success : function(data) {
                $("#tablaalumno").html(data);
            },
         
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        });
    }

    $('#tabla tbody').on('click','.detalle',function(){
        $('#tablaalumno').removeClass('hidden');

        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        var nombre=tabla.cell(fila,0).data();
        var apellidos=tabla.cell(fila,1).data();
        var clave=tabla.cell(fila,5).data();

        $('#tablaalumno').removeClass('hidden');
        
        detalles(clave);
        $(".titulo").html('<strong>Detalle del alumno '+nombre+' '+apellidos+'</strong>');
        $('#ventanaDetalle').modal({show:true, backdrop:false, keyboard:false});
    });

    $('#fechanac').datepicker({
        format: "yyyy-mm-dd",
        startDate: "1947-01-01",
        endDate: "2002-01-01",
        startView: 1,
        maxViewMode: 2,
        language: "es",
        autoclose: true  
    });
});
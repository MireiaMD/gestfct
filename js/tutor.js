$(function() {
    
    var tr;
    var fila;
    
    var tabla=$('#tabla').DataTable( {
        responsive: true,
        "language": {"url": "../ajax/LangSpanish.txt" },
        //para coger de bd
        "ajax": 'cargardata.php?opc=3',
		
        "columns": [
            { "data": "nombre" },
            { "data": "apellidos" },
            { "data": "dni" },
            { "data": "id","class":"hidden" },
            {data: null,
                orderable:false,
                searchable: false,
                sortable:false,
                defaultContent: "<div class='text-center'><button type='button' class='detalle btn btn-deta btn-xs' title='Detallar registro'><i class='fa fa-paperclip'></i></button>&nbsp;<button type='button' class='editar btn btn-xs btn-edit' title='Editar registro'><i class='fa fa-pencil-square-o'></i></button>&nbsp;<button type='button' class='borrar btn btn-danger btn-xs' title='Borrar registro'><i class='fa fa-trash-o'></i></button></div>" 
            }
        ],

        dom: "<'row'<'form-inline'<'col-sm-3'l>>><rt><'row'<'form-inline'<'col-sm-7'B><'col-sm-5'f>>><rt><'row'<'form-inline'<'col-sm-6'i><'col-sm-6'p>>>",
        
        buttons:[
            'excel', 'pdf',
            {
                text:'Añadir',
                titleAttr:'Añadir',
                className: 'btn btn-anadir',
                action: function(){ anadir(); }
            }
        ]
    } );

    var comprobar=function(variable){
        var nulo=false;
        if(variable==""){
        nulo=true;
        }
        return nulo;
    }

    var menorDiez=function(variable){
        if (parseInt(variable)<10){
            return "0"+variable;
        }
        else 
            return variable;
    }

    function nif(dni) {
      var numero;
      var letr;
      var letra;
      var expresion_regular_dni;
     
      expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
     
      if(expresion_regular_dni.test (dni) == true){
         numero = dni.substr(0,dni.length-1);
         letr = dni.substr(dni.length-1,1);
         numero = numero % 23;
         letra='TRWAGMYFPDXBNJZSQVHLCKET';
         letra=letra.substring(numero,numero+1);
        if (letra!=letr.toUpperCase()) {
           return 0;
         }
         else{
           return 1;
         }
      }
      else{
         return 2;
      }
    }

    var generafecha=function(){
        var f=new Date();
        var ano=f.getFullYear();
        var mes=f.getMonth()+1;
        var dia=f.getDate();

        var fecha=("0"+dia).slice(-2)+"/"+menorDiez(mes)+"/"+ano;

        return fecha;
    }

    var llamarAnadir=function(){
        var opc="1";
        $.ajax({
            url : 'gesttutor.php',

            data : $('#miFormulario').serialize()+"&opc"+"="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },       	

            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarEditar=function(){
        var opc="3";
        $.ajax({
            url : 'gesttutor.php',

            data:$('#miFormulario').serialize()+"&opc"+"="+opc,
            
            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },       	
            
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarBorrar=function(){
        var opc="4";
        $.ajax({
            url : 'gesttutor.php',

            data : $('#miFormulario').serialize()+"&opc"+"="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          
           
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#btnSubmit').on('click',function(e){
        e.preventDefault();
        if($("#myModalLabel").html()=="<strong>Añadir Tutor</strong>"){
        	
            valores = {
                "nombre": $('#nombre').val(),
                "apellidos": $('#apellidos').val(),
                "dni": $('#dni').val(),
                "id":$('#clave').val()
        	};
            valores.alta=generafecha();	        	

            var checkNif=nif($('#dni').val());

        	if(comprobar(valores.nombre) || comprobar(valores.apellidos) || comprobar(valores.dni) || comprobar($('#email').val())){
                swal({
                      title: "Error",
                      text: "Los campos: Nombre, Apellidos, Correo electronico y DNI no pueden quedar vacios",
                      type: "error",
                      confirmButtonText: "Cerrar"
                });
        	}
            else if(checkNif==0){
                swal({
                  title: "Error",
                  text: "Dni erroneo, la letra del NIF no se corresponde",
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
            }
            else if(checkNif==2){
                swal({
                  title: "Error",
                  text: "Dni erroneo, formato no válido",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        	else{
            	//llamar a funcion que llama al php
            	llamarAnadir();
        	}
        }
        else if($("#myModalLabel").html()=="<strong>Borrar usuario</strong>"){
            llamarBorrar();
        }
        else {
       		//llamar a funcion que llama al php
        	llamarEditar();
        }
        
        $('#miVentana').modal('hide');

    });
    
    var anadir= function(){
        $(".titulo").html("<strong>Añadir Tutor</strong>");
        $("#btnSubmit").html("<strong>Guarda Nuevo</strong>");
        $("#btnSubmit").attr("name","anadir");
        $('#grupobaja').addClass('hidden');
        $('#grupousuario').addClass('hidden');
        $('#grupoalta').removeClass('hidden');
        $('#grupomail').removeClass('hidden');
        $("#btnSubmit").addClass('btn-primary');
        $("#btnSubmit").removeClass('btn-danger');

        $("#miFormulario")[0].reset();
        var alta=generafecha(); 
        $('#alta').val(alta);

        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});

    };

    var rellena=function(id){
        var opc="2";
        $('#email').val("");
        $.ajax({
            url : 'gesttutor.php',

            data : {"id": id,"opc":opc},
            
            type : 'POST',

            dataType : 'text',

            success: function(data) {
                $('#email').val(data);
            },          
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#tabla tbody').on('click','.editar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();
        
        var usuario=tabla.cell(fila,0).data()+" "+tabla.cell(fila,1).data();
        $(".titulo").html("<strong>Editar Usuario "+usuario+"</strong>");
        $("#btnSubmit").html("<strong>Guarda Cambios</strong>");
        $("#btnSubmit").attr("name","editar");
        $('#grupousuario').addClass('hidden');
        $('#grupobaja').addClass('hidden');
        $('#grupoalta').addClass('hidden');
        $('#grupomail').removeClass('hidden');
        $("#btnSubmit").addClass('btn-primary');
        $("#btnSubmit").removeClass('btn-danger');

        var id=tabla.cell(fila,3).data();

        rellena(id);       

        var valores=[
            $('#nombre').val(tabla.cell(fila,0).data()),
            $('#apellidos').val(tabla.cell(fila,1).data()),
            $('#dni').val(tabla.cell(fila,2).data()),
            $('#clave').val(id),
        ];
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});

    });

    $('#tabla tbody').on('click','.borrar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();
        $(".titulo").html("<strong>Borrar usuario</strong>");
        $("#btnSubmit").html("<strong>Borrar registro</strong>");
        $("#btnSubmit").attr("name","borrar");
        $('#grupobaja').removeClass('hidden');
        $('#grupoalta').addClass('hidden');
        $('#grupomail').addClass('hidden');
        $("#btnSubmit").removeClass('btn-primary');
        $("#btnSubmit").addClass('btn-danger');

        var baja=generafecha();
        var valores=[
        $('#clave').val(tabla.cell(fila,3).data()),
        $('#nombre').val(tabla.cell(fila,0).data()),
        $('#apellidos').val(tabla.cell(fila,1).data()),
        $('#dni').val(tabla.cell(fila,2).data()),
        $('#baja').val(baja)
        ];
                
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});
    });

    var detalles=function (clave){
        var opc="5";
        $.ajax({
            url : 'gesttutor.php',
         
            data : { "id": clave, "opc": opc },
         
            type : 'POST',
         
            dataType : 'html',
         
            success : function(data) {
                $("#tabladetalle").html(data);
            },
         
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        });
    }

    $('#tabla tbody').on('click','.detalle',function(){        
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        var tutor=tabla.cell(fila,0).data()+" "+tabla.cell(fila,1).data();
        var clave= tabla.cell(fila,3).data();

        detalles(clave);
        $(".titulo").html('<strong>Detalle del tutor: '+tutor+'</strong>');
        $('#ventanaDetalle').modal({show:true, backdrop:false, keyboard:false});
    });
});
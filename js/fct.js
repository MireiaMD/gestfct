$(function() {
    
    var tr;
    var fila;
    
    var tabla=$('#tabla').DataTable( {
        responsive: true,
        "language": {"url": "../ajax/LangSpanish.txt" },
        //para coger de bd
        "ajax": 'cargardata.php?opc=7',
               
        "columns": [
            { "data": "alumno" },
            { "data": "docente" },
            { "data": "inicio" },
            { "data": "fin" },
            { "data": "horas" },
            { "data": "vacante" },
            { "data": "id","class":"hidden" },
            {data: null,
                orderable:false,
                searchable: false,
                sortable:false,
                defaultContent: "<div class='text-center'><button type='button' class='detalle btn btn-deta btn-xs' title='Detallar registro'><i class='fa fa-paperclip'></i></button><!--&nbsp;<button type='button' class='editar btn btn-xs btn-edit' title='Editar registro'><i class='fa fa-pencil-square-o'></i></button>--></div>" 
            }
        ],

        dom: "<'row'<'form-inline'<'col-sm-3'l>>><rt><'row'<'form-inline'<'col-sm-7'B><'col-sm-5'f>>><rt><'row'<'form-inline'<'col-sm-6'i><'col-sm-6'p>>>",
        
        buttons:[
            'excel', 'pdf',
            {
                text:'Añadir',
                titleAttr:'Añadir',
                className: 'btn btn-anadir',
                action: function(){ anadir(); }
            }
        ]
    });
    
    var comprobar=function(variable){
        var nulo=false;
        if(variable==""){
        nulo=true;
        }
        return nulo;
    }
    
    var llamarAnadir=function(){
        var opc="1";
        $.ajax({
            url : 'gestfct.php',

            data : $('#miFormulario').serialize()+"&opc="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          

            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#btnSubmit').on('click',function(e){
        e.preventDefault();
        if($("#myModalLabel").html()=="<strong>Añadir FCT</strong>"){
            
            valores = {
                "alumno": $('#alumno').val(),
                "inicio":$('#inicio').val(),
            };
            
            if(comprobar(valores.alumno) || comprobar(valores.inicio) || comprobar(valores.vacante)){

                swal({
                  title: "Error",
                  text: "Alguno de los campos está vacio",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
                
            }
            else{
                //llamar a funcion que llama al php
                llamarAnadir();
            }
        }
        
        $('#miVentana').modal('hide');

    });
    
    var anadir= function(){
        $('#miFormulario')[0].reset();

        $(".titulo").html("<strong>Añadir FCT</strong>");
        $("#btnSubmit").html("<strong>Guarda Nuevo</strong>");
        $("#btnSubmit").addClass('btn-primary');
        
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});

    };

    var detalles=function (clave){
        var opc="2";
        $.ajax({
            url : 'gestfct.php',
         
            data : { "id": clave,"opc":opc},
         
            type : 'POST',
         
            dataType : 'html',
         
            success : function(data) {
                $("#tabladetalle").html(data);
            },
         
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        });
    };
    $('#tabla tbody').on('click','.detalle',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }

        fila=tabla.row(trActual).index();

        var nombre=tabla.cell(fila,0).data();
        var clave= tabla.cell(fila,6).data();
        detalles(clave);

        $(".titulo").html('<strong>Diario de la fct de '+nombre+'</strong>');

        $('#ventanaDetalle').modal({show:true, backdrop:false, keyboard:false});
    });
    
    $('#inicio').datepicker({
        format: "yyyy-mm-dd",
        startView: 0,
        maxViewMode: 2,
        language: "es",
        autoclose: true,
        assumeNearbyYear: 20,
        daysOfWeekDisabled: "0",
        startDate: '+0d',
        todayBtn: "linked",
        todayHighlight: true,
        datesDisabled: ['2017-01-01', '2017-01-06','2017-03-28', '2017-04-13','2017-04-14','2017-05-01','2017-05-17','2017-07-25','2017-08-15','2017-08-16','2017-10-12','2017-11-01','2017-12-06','2017-12-08','2017-12-25','2018-01-01', '2018-01-06','2018-03-28','2018-03-29','2018-03-30','2018-05-01','2018-05-17']
    });
});
$(function() {
    
    var tr;
    var fila;
    
    var tabla=$('#tabla').DataTable( {
        responsive: true,
        "language": {"url": "../ajax/LangSpanish.txt" },
        //para coger de bd
        "ajax": 'cargardata.php?opc=2',
		
        "columns": [
            { "data": "email" },
            { "data": "id","class":"hidden" },
            { "data": "usuario" },
            { "data": "tipo" },
            { "data": "baja" },
            {data: null,
                orderable:false,
                searchable: false,
                sortable:false,
                defaultContent: "<div class='text-center'><button type='button' class='detalle btn btn-deta btn-xs' title='Detallar registro'><i class='fa fa-paperclip'></i></button></div>" 
            }
        ],

        dom: "<'row'<'form-inline'<'col-sm-3'l>>><rt><'row'<'form-inline'<'col-sm-7'B><'col-sm-5'f>>><rt><'row'<'form-inline'<'col-sm-6'i><'col-sm-6'p>>>",
        
        buttons:[
            'excel', 'pdf'
        ]
    });
    
    var detalles=function (clave,tipo){
        var opc="1";
        $.ajax({
            url : 'gestusuinact.php',
         
            data : { "id": clave,"opc": opc},
         
            type : 'POST',
         
            dataType : 'html',
         
            success : function(data) {
                
                if(tipo==1){
                    $("#tablaadmin").html(data);
                }
                if(tipo==4){
                    $("#tablaalumno").html(data);
                }
                else{
                    $("#tabladocente").html(data);
                }
            },
         
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        });
    }

    $('#tabla tbody').on('click','.detalle',function(){
        $('#tabladocente').addClass('hidden');
        $('#tablaalumno').addClass('hidden');
        $('#tablaadmin').addClass('hidden');

        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        var usuario=tabla.cell(fila,2).data();
        var clave= tabla.cell(fila,1).data();
        var tipo= tabla.cell(fila,3).data();

        if(tipo==1){
            $('#tablaadmin').removeClass('hidden');
        }
        else if(tipo==4){
            $('#tablaalumno').removeClass('hidden');
        }
        else{
            $('#tabladocente').removeClass('hidden');
        }
        detalles(clave,tipo);
        $(".titulo").html('<strong>Detalle del usuario '+usuario+'</strong>');
        $('#ventanaDetalle').modal({show:true, backdrop:false, keyboard:false});
    });
});
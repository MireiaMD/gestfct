$(function() {
    
    var tr;
    var fila;
    
    var tabla=$('#tabla').DataTable( {
        responsive: true,
        "language": {"url": "../ajax/LangSpanish.txt" },
        //para coger de bd
        "ajax": 'cargardata.php?opc=1',
        
        "columns": [
            { "data": "email" },
            { "data": "id","class":"hidden" },
            { "data": "usuario" },
            { "data": "tipo" },
            { "data": "alta" },
            {data: null,
                orderable:false,
                searchable: false,
                sortable:false,
                defaultContent: "<div class='text-center'><button type='button' class='detalle btn btn-deta btn-xs' title='Detallar registro'><i class='fa fa-paperclip'></i></button>&nbsp;<button type='button' class='editar btn btn-xs btn-edit' title='Editar registro'><i class='fa fa-pencil-square-o'></i></button>&nbsp;<button type='button' class='borrar btn btn-danger btn-xs' title='Borrar registro'><i class='fa fa-trash-o'></i></button></div>" 
            }
        ],

        dom: "<'row'<'form-inline'<'col-sm-3'l>>><rt><'row'<'form-inline'<'col-sm-7'B><'col-sm-5'f>>><rt><'row'<'form-inline'<'col-sm-6'i><'col-sm-6'p>>>",
        
        buttons:[
            'excel', 'pdf',
            {
                text:'Añadir',
                titleAttr:'Añadir',
                className: 'btn btn-anadir',
                action: function(){ anadir(); }
            }
        ]
    });

    var comprobar=function(variable){
        var nulo=false;
        if(variable==""){
        nulo=true;
        }
        return nulo;
    }

    var menorDiez=function(variable){
        if (parseInt(variable)<10){
            return "0"+variable;
        }
        else 
            return variable;
    }

    function nif(dni) {
      var numero;
      var letr;
      var letra;
      var expresion_regular_dni;
     
      expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
     
      if(expresion_regular_dni.test (dni) == true){
         numero = dni.substr(0,dni.length-1);
         letr = dni.substr(dni.length-1,1);
         numero = numero % 23;
         letra='TRWAGMYFPDXBNJZSQVHLCKET';
         letra=letra.substring(numero,numero+1);
        if (letra!=letr.toUpperCase()) {
           return 0;
         }
         else{
           return 1;
         }
      }
      else{
         return 2;
      }
    }

    var generafecha=function(){
        var f=new Date();
        var ano=f.getFullYear();
        var mes=f.getMonth()+1;
        var dia=f.getDate();

        var fecha=("0"+dia).slice(-2)+"/"+menorDiez(mes)+"/"+ano;

        return fecha;
    }

    $("select[name=tipo]").change(function(){
        if($("#myModalLabel").html()=="<strong>Añadir Usuario</strong>"){
            if($('select[name=tipo]').val()==4){
                $('#alumno').removeClass('hidden');
                $('#docente').removeClass('hidden');  
            }
            else if($('select[name=tipo]').val()==1){
                $('#alumno').addClass('hidden');
                $('#docente').addClass('hidden');
            }
            else{
                $('#alumno').addClass('hidden');
                $('#docente').removeClass('hidden');
            }
        }
    });

    var llamarAnadir=function(){
        var opc="1";
        $.ajax({
            url : 'gestusuact.php',

            data : $('#miFormulario').serialize()+"&opc"+"="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          

            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarEditar=function(){
        var opc="3";
        $.ajax({
            url : 'gestusuact.php',

            data:$('#miFormulario').serialize()+"&opc"+"="+opc,
            
            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          
            
            error : function(xhr, status) {
                 swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarBorrar=function(){
        var opc="4";
        alert("pulsaste borrar");
        $.ajax({
            url : 'gestusuact.php',

            data : $('#miFormulario').serialize()+"&opc"+"="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                tabla.ajax.reload();
            },          
           
            error : function(xhr, status) {
                 swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#btnSubmit').on('click',function(e){
        e.preventDefault();
        if($("#myModalLabel").html()=="<strong>Añadir Usuario</strong>"){
            
            valores = {
                "email": $('#email').val(),
                "tipo":$('#tipo').val(),
                "nombre":$('#nombre').val(),
                "apellidos":$('#apellidos').val(),
                "telefono":$('#telefono').val(),
                "dni":$('#dni').val()
            };
            valores.alta=generafecha();
            var checkNif=nif($('#dni').val());

            if(valores.tipo==1){
                if(comprobar(valores.email)||comprobar(valores.nombre)||comprobar(valores.apellidos)){
                    swal({
                      title: "Error",
                      text: "Los campos: Nombre, Apellidos y Correo electronico no pueden quedar vacios",
                      type: "error",
                      confirmButtonText: "Cerrar"
                    });
                }
                else{
                    llamarAnadir();
                }
            }
            else if((valores.tipo)==4){
                if(comprobar(valores.email)||comprobar(valores.nombre)||comprobar(valores.apellidos)||comprobar(valores.telefono)||comprobar(valores.dni)){
                    swal({
                      title: "Error",
                      text: "Los campos: Nombre, Apellidos, Correo electronico, Telefono y DNI no pueden quedar vacios",
                      type: "error",
                      confirmButtonText: "Cerrar"
                    });
                }
                else if(checkNif==0){
                    swal({
                      title: "Error",
                      text: "Dni erroneo, la letra del NIF no se corresponde",
                      type: "info",
                      confirmButtonText: "Cerrar"
                    });
                }
                else if(checkNif==2){
                    swal({
                      title: "Error",
                      text: "Dni erroneo, formato no válido",
                      type: "error",
                      confirmButtonText: "Cerrar"
                    });
                }
                else{
                    llamarAnadir();
                }
            }
            else if((valores.tipo)==2||(valores.tipo)==3){
                if(comprobar(valores.email)||comprobar(valores.nombre)||comprobar(valores.apellidos)||comprobar(valores.dni)){
                    swal({
                      title: "Error",
                      text: "Los campos: Nombre, Apellidos, Correo electronico y DNI no pueden quedar vacios",
                      type: "error",
                      confirmButtonText: "Cerrar"
                    });
                }
                else if(checkNif==0){
                    swal({
                      title: "Error",
                      text: "Dni erroneo, la letra del NIF no se corresponde",
                      type: "info",
                      confirmButtonText: "Cerrar"
                    });
                }
                else if(checkNif==2){
                    swal({
                      title: "Error",
                      text: "Dni erroneo, formato no válido",
                      type: "error",
                      confirmButtonText: "Cerrar"
                    });
                }
                else{
                    llamarAnadir();
                }
            }
        }
        else if($("#myModalLabel").html()=="<strong>Borrar usuario</strong>"){
        	llamarBorrar();
            $('#todos').removeClass('hidden');
        }
        else {
            llamarEditar();
        }
        
        $('#miVentana').modal('hide');

    });
    
    var anadir= function(){
        $('#miFormulario')[0].reset();

        $(".titulo").html("<strong>Añadir Usuario</strong>");
        $("#btnSubmit").html("<strong>Guarda Nuevo</strong>");
        $("#btnSubmit").attr("name","anadir");
        $('#grupobaja').addClass('hidden');
        $('#todos').removeClass('hidden');
        $('#alumno').addClass('hidden');
        $('#alumnoe').addClass('hidden');
        $('#docente').addClass('hidden');
        $('#grupousuario').addClass('hidden');
        $('#grupoalta').removeClass('hidden');
        $("#btnSubmit").addClass('btn-primary');
        $("#btnSubmit").removeClass('btn-danger');
        $("#grupotutorA").removeClass('hidden');
        $("#grupotutorB").addClass('hidden');
        $("#grupotipoA").removeClass('hidden');
        $("#grupotipoB").addClass('hidden');

        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        var tipo= tabla.cell(fila,3).data();
        var alta=generafecha(); 
        
        $('#alta').val(alta);
        
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});

    };

    var rellena=function(id,tipo){
        var opc="2";
        $('#miFormulario')[0].reset();
        $.ajax({
            url : 'gestusuact.php',

            data : {"id": id,"tipo": tipo,"opc": opc},
            
            type : 'POST',

            dataType : 'json',

            success: function(data) {
                $('#nombre').val(data.nombre);
                $('#apellidos').val(data.apellidos);
                $('#dni').val(data.dni);
                $('#fechanac').val(data.fechanac);
                $('#titulo').val(data.titulo);
                $('#direccion').val(data.direccion);
                $('#codpostal').val(data.codpostal);
                $('#poblacion').val(data.poblacion);
                $('#provincia').val(data.provincia);
                $('#telefono').val(data.telefono);
                $('#estudiosant').val(data.estudiosant);
                $('#experiencia').val(data.experiencia);
                $('#preferencias').val(data.preferencias);
                $('#tutor').val(data.tutor);
            },          
            error : function(xhr, status) {
                 swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#tabla tbody').on('click','.editar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();
        
        var usuario=tabla.cell(fila,2).data();
        $(".titulo").html("<strong>Editar Usuario "+usuario+"</strong>");
        $("#btnSubmit").html("<strong>Guarda Cambios</strong>");
        $("#btnSubmit").attr("name","editar");
        $('#grupousuario').addClass('hidden');
        $('#grupobaja').addClass('hidden');
        $('#grupoalta').addClass('hidden');
        $("#tipob").attr("disabled","");
        $("#tutor").attr("disabled","");
        $("#btnSubmit").addClass('btn-primary');
        $("#btnSubmit").removeClass('btn-danger');
        $("#grupotipoA").addClass('hidden');
        $("#grupotipoB").removeClass('hidden');
        $("#grupotutorA").addClass('hidden');
        $("#grupotutorB").removeClass('hidden');
        tutor='<label class="col-xs-4" for="tutor">Tutor</label><input type="text" class="col-xs-7" name="tutor" id="tutor" placeholder="Tutor" disabled>';
        $('#grupotutorB').html(tutor);
        
        var id=tabla.cell(fila,1).data();
        var tipopre=tabla.cell(fila,3).data();
        switch (tipopre) {
            case "alumno":
                tipo=4;
                break;
            case "profesor":
                tipo=3;
                break;
            case "tutor":
                tipo=2;
                break;
            case "administrador":
                tipo=1;
                break;
        }

        rellena(id,tipo);       

        var valores=[
            $('#email').val(tabla.cell(fila,0).data()),
            $('#clave').val(tabla.cell(fila,1).data()),
            $('#usuario').val(tabla.cell(fila,2).data()),
            $('#tipob').val(tipopre),
            $('#tipo').val(tipo),
        ];
            $('#alumnoe').addClass('hidden');
            $('#alumno').addClass('hidden');
            $('#docente').addClass('hidden');
            $('#todos').removeClass('hidden');
        if(tipo==4){ 
            $('#alumno').removeClass('hidden');
            $('#alumnoe').removeClass('hidden');
            $('#docente').removeClass('hidden');
        }
        else if(tipo==2||tipo==3){
            $('#alumnoe').addClass('hidden');
            $('#alumno').addClass('hidden');
            $('#docente').removeClass('hidden');
        }
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});
    });

    $('#tabla tbody').on('click','.borrar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        $(".titulo").html("<strong>Borrar usuario</strong>");
        $("#btnSubmit").html("<strong>Borrar registro</strong>");
        $("#btnSubmit").attr("name","borrar");
        $('#grupobaja').removeClass('hidden');
        $('#grupoalta').addClass('hidden');
        $('#todos').addClass('hidden');
        $('#alumno').addClass('hidden');
        $('#alumnoe').addClass('hidden');
        $('#docente').addClass('hidden');
        $('#grupousuario').removeClass('hidden');
        $("#btnSubmit").removeClass('btn-primary');
        $("#btnSubmit").addClass('btn-danger');
        $("#tipob").attr("disabled","");
        $("#grupotipoA").addClass('hidden');
        $("#grupotipoB").removeClass('hidden');
        
        var baja=generafecha();
        var tipopre=tabla.cell(fila,3).data();
        /*var tipo=0;
        switch (tipopre) {
            case "alumno":
                tipo=4;
                break;
            case "profesor":
                tipo=3;
                break;
            case "tutor":
                tipo=2;
                break;
            case "administrador":
                tipo=1;
                break;
        }*/
        var valores=[
        $('#clave').val(tabla.cell(fila,1).data()),
        $('#email').val(tabla.cell(fila,0).data()),
        $('#usuario').val(tabla.cell(fila,2).data()),
        $('#tipob').val(tipopre),
        $('#baja').val(baja)
        ];     
        $('#miVentana').modal({show:true, backdrop:false, keyboard:false});
    });

    var detalles=function (clave,tipo){
        var opc="5";
        $.ajax({
            url : 'gestusuact.php',
         
            data : { "id": clave,"opc": opc},
         
            type : 'POST',
         
            dataType : 'html',
         
            success : function(data) {
                
                if(tipo==1){
                    $("#tablaadmin").html(data);
                }
                if(tipo==4){
                    $("#tablaalumno").html(data);
                }
                else{
                    $("#tabladocente").html(data);
                }
            },
         
            error : function(xhr, status) {
                 swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        });
    }

    $('#tabla tbody').on('click','.detalle',function(){
        $('#tabladocente').addClass('hidden');
        $('#tablaalumno').addClass('hidden');
        $('#tablaadmin').addClass('hidden');

        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }
        fila=tabla.row(trActual).index();

        var usuario=tabla.cell(fila,2).data();
        var clave= tabla.cell(fila,1).data();
        var tipo= tabla.cell(fila,3).data();

        if(tipo==1){
            $('#tablaadmin').removeClass('hidden');
        }
        else if(tipo==4){
            $('#tablaalumno').removeClass('hidden');
        }
        else{
            $('#tabladocente').removeClass('hidden');
        }
        detalles(clave,tipo);
        $(".titulo").html('<strong>Detalle del usuario '+usuario+'</strong>');
        $('#ventanaDetalle').modal({show:true, backdrop:false, keyboard:false});
    });

    $('#fechanac').datepicker({
        format: "yyyy-mm-dd",
        startDate: "1947-01-01",
        endDate: "2002-01-01",
        startView: 1,
        maxViewMode: 2,
        language: "es",
        autoclose: true  
    });
});
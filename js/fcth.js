$(function() {
    
    var tr;
    var fila;

    var fcth=$('#fcth').DataTable( {
        responsive: true,
        "language": {"url": "../ajax/LangSpanish.txt" },
        //para coger de bd
        "ajax": 'cargardata.php?opc=8',
        
        "columns": [
            { "data": "fecha" },
            { "data": "numhoras" },
            { "data": "observaciones" },
            { "data": "id","class":"hidden" },
            { "data": "idfct","class":"hidden" },
            {data: null,
                orderable:false,
                searchable: false,
                sortable:false,
                defaultContent: "<div class='text-center'><button type='button' class='editar btn btn-xs btn-edit' title='Editar registro'><i class='fa fa-pencil-square-o'></i></button></div>" 
            }
        ],

        dom: "<'row'<'form-inline'<'col-sm-3'l>>><rt><'row'<'form-inline'<'col-sm-7'B><'col-sm-5'f>>><rt><'row'<'form-inline'<'col-sm-6'i><'col-sm-6'p>>>",
        
        buttons:[
            'excel', 'pdf',
            {
                text:'Nuevo',
                titleAttr:'Nuevo',
                className: 'btn btn-anadir',
                action: function(){ nuevo(); }
            }
        ]
    });
    
    var comprobar=function(variable){
        var nulo=false;
        if(variable==""){
        nulo=true;
        }
        return nulo;
    }
    
    var llamarNuevo=function(){
        var opc="3";
        $.ajax({
            url : 'gestfct.php',

            data : $('#formfcth').serialize()+"&opc="+opc,

            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                fcth.ajax.reload();
            },          

            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    var llamarEditar=function(){
        var opc="4";
        $.ajax({
            url : 'gestfct.php',

            data:$('#formfcth').serialize()+"&opc="+opc,
            
            type : 'POST',

            success: function(data) {
                swal({
                  title: "",
                  text: data,
                  type: "info",
                  confirmButtonText: "Cerrar"
                });
                fcth.ajax.reload();
            },          
            
            error : function(xhr, status) {
                swal({
                  title: "Error",
                  text: "Disculpe, existió un problema",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            }
        })
    };

    $('#btnSubmith').on('click',function(e){
        e.preventDefault();
        if($("#tituloh").html()=="<strong>Añadir registro diario</strong>"){
            
            valores = {
                "fecha": $('#fecha').val(),
                "numhoras": $('#numhoras').val(),
                "observaciones":$('#observaciones').val(),
            };
            
            if(comprobar(valores.fecha) || comprobar(valores.numhoras) || comprobar(valores.observaciones)){

                swal({
                  title: "Error",
                  text: "Alguno de los campos está vacio",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
                
            }
            else{
                //llamar a funcion que llama al php
                llamarNuevo();
            }
        }
        else if($("#tituloh").html()=="<strong>Editar registro</strong>"){
            //llamar a funcion que llama al php
            llamarEditar();
        }
        
        $('#ventfcth').modal('hide');

    });

    var nuevo= function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }

        fila=fcth.row(trActual).index();

        $('#formfcth')[0].reset();
        $("#tituloh").html("<strong>Añadir registro diario</strong>");
        $("#btnSubmith").html("<strong>Guarda nuevo</strong>");
        $("#clavefct").val(fcth.cell(fila,4).data());
        $('#ventfcth').modal({show:true, backdrop:false, keyboard:false});
    };

    $('#cerrarfcth').on('click',function(){
        $('#ventfcth').modal('hide');
    });

    $('#fcth tbody').on('click','.editar',function(){
        tr=$(this).closest("tr");
        if(tr.attr('class')=='child'){
            previo=tr.prev();
            trActual=previo;
        }
        else{
            trActual=tr;
        }

        fila=fcth.row(trActual).index();
        
        $('#formfcth')[0].reset();
        $("#tituloh").html("<strong>Editar registro</strong>");
        $("#btnSubmith").html("<strong>Guarda Cambios</strong>");

        var valores=[
            $('#fecha').val(fcth.cell(fila,0).data()),
            $('#numhoras').val(fcth.cell(fila,1).data()),
            $('#observaciones').val(fcth.cell(fila,2).data()),
            $('#clave').val(fcth.cell(fila,3).data()),
            $('#clavefct').val(fcth.cell(fila,4).data()),
        ];
        
        $('#ventfcth').modal({show:true, backdrop:false, keyboard:false});
    });

    $('#fecha').datepicker({
        format: "yyyy-mm-dd",
        startView: 0,
        maxViewMode: 1,
        language: "es",
        autoclose: true,
        assumeNearbyYear: 20,
        daysOfWeekDisabled: "0",
        startDate: '-6m',
        endDate: '+6m',
        todayBtn: "linked",
        todayHighlight: true,
        datesDisabled: ['2017-01-01', '2017-01-06','2017-03-28', '2017-04-13','2017-04-14','2017-05-01','2017-05-17','2017-07-25','2017-08-15','2017-08-16','2017-10-12','2017-11-01','2017-12-06','2017-12-08','2017-12-25','2018-01-01', '2018-01-06','2018-03-28','2018-03-29','2018-03-30','2018-05-01','2018-05-17']
    });
});
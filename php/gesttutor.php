<?php
require_once('comprobar.php');
$opc=$_POST['opc'];
switch ($opc) {
	/*Añadir tutor*/
	case '1':
		require_once('clasespoo/Tutor.php');
		$mail=$_POST['email'];
		$nombre=$_POST['nombre'];
		$apellidos=$_POST['apellidos'];
		$dni=$_POST['dni'];
		$log="";
		if(comprmail($mail)){
			if(comprnombre($nombre)){
				if(comprapell($apellidos)){
					if (comprdni($dni)) {
			            $tutor = new Tutor($mail, $nombre, $apellidos, $dni);
			            $log = $tutor->crear($log);
			        } else {
			            $log .= "el dni no es correcto\r\n";
			        }
			    }
				else{
					$log.= "el tamaño de apellidos supera el limite\r\n";
				}
			}
			else{
				$log.= "el tamaño de nombre supera el limite\r\n";
			}
		}
		else{
			$log.= "el mail no es correcto\r\n";
		}
		echo $log;
		break;
	/*rellenar datos para formu editar*/
	case '2':
		require_once('BaseDatos.php');
		$id=$_POST['id'];
		$mysqli=conectar();
		$sql="SELECT email FROM usuarios INNER JOIN docentes ON usuarios.id=docentes.id WHERE usuarios.id='$id'";
		$resultado=$mysqli->query($sql);
		if($mysqli->errno){
			die('Esto va mal' .$mysqli->error);
		}
		$registro=$resultado->fetch_assoc();
		$email=$registro['email'];
		desconectar($mysqli);
		echo $email;
		break;
	/*editar tutor*/
	case '3':
		require_once('clasespoo/Tutor.php');

	    $id=$_POST['clave'];
		$mail=$_POST['email'];
		$nombre=$_POST['nombre'];
		$apellidos=$_POST['apellidos'];
		$dni=$_POST['dni'];
		$log="";
		if(comprmail($mail)){
			if(comprnombre($nombre)){
				if(comprapell($apellidos)){
					if (comprdni($dni)) {
				        $tutor = new Tutor($mail, $nombre, $apellidos, $dni);
				        $log = $tutor->modificar($id, $log);
			        } else {
			            $log .= "el dni no es correcto\r\n";
			        }
			    }
				else{
					$log.= "el tamaño de apellidos supera el limite\r\n";
				}
			}
			else{
				$log.= "el tamaño de nombre supera el limite\r\n";
			}
		}
		else{
			$log.= "el mail no es correcto\r\n";
		}
		echo $log;
		break;
	/*eliminar tutor*/
	case '4':
		require_once('clasespoo/Usuario.php');

		$id=$_POST['clave'];
		$email="";

		$usuario=new Usuario($email);

		$log=$usuario->eliminar($id,$log);
		echo $log;
		break;
	/*ver detalle del tutor*/
	case '5':
	require_once('BaseDatos.php');
	
	if( isset($_POST['id']) ){
		$mysqli=conectar();
		$id=$_POST['id'];
		$sql="SELECT * FROM usuarios WHERE id = '$id'";
		$resultado = $mysqli->query($sql);
		$fila = $resultado->fetch_assoc();
		$pre1="<th>ID</th>
           <th>Correo electronico</th>
           <th>Usuario</th>
           <th>Fecha de alta</th>";
		$pre2="<td>".$fila['id']."</td><td>".$fila['email']."</td><td>".$fila['usuario']."</td><td>".$fila['alta']."</td>";
		
  		$dev= '<table class="table table-hover table-bordered table-condensed" >
		      <thead><tr>'.$pre1.'</tr></thead>
		      <tbody><tr>'.$pre2.'</tr></tbody></table>';
		echo $dev;
		desconectar($mysqli);
	}
		break;
}
?>
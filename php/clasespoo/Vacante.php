<?php
	require_once ('BaseDatos.php');
	
	class Vacante{
		private $id="";
		private $idempresa="";
		private $requisitos="";//en bd requisitostec
		private $curso="";//en bd curso_escolar

		public function __construct($idempresa,$requisitos,$curso){
			$mysqli=conectar();
			$this->idempresa=$mysqli->real_escape_string($idempresa);
			$this->requisitos=$mysqli->real_escape_string($requisitos);
			$this->curso=$mysqli->real_escape_string($curso);
			desconectar($mysqli);
		}

		public function __get($variable){
			if($variable=="idempresa"||$variable=="requisitos"||$variable=="curso"){
				return $this->$variable;
			}
		}

		public function __set($variable,$valor){
			if($variable=="idempresa"||$variable=="requisitos"||$variable=="curso"){
				$this->$variable=$valor;
			}
		}
		
		public function __toString(){
			return "Idempresa=$this->idempresa, Requisitos tecnicos=$this->requisitos, Curso escolar=$this->curso";
		}

		public function crear($log){
			$mysqli=conectar();

			$sql="INSERT INTO `vacantes` (`idempresa`, `requisitostec`, `curso_escolar`) VALUES ('$this->idempresa', '$this->requisitos', '$this->curso')";

			$mysqli->query($sql);
			if($mysqli->errno) {
				$log.="Error en consulta\r\n";
			}
			
			desconectar($mysqli);
			
			$log.="registro guardado\r\n";
			return $log;
		}		
		
		public function modificar($id,$log){
			$mysqli=conectar();

			$sql = "UPDATE vacantes SET idempresa = '".$this->idempresa."', requisitostec = '".$this->requisitos."', curso_escolar = '".$this->curso."' where id=$id";
			
			$mysqli->query($sql);  
			if($mysqli->connect_errno){
				$log.="Error en consulta\r\n";
			}

			desconectar($mysqli);
			$log.="Registro guardado\r\n";		
			return $log;
		}
	}
?>
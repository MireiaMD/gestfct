<?php
	require_once ('BaseDatos.php');
	
	class Fct{
		private $id="";
		private $alumno="";
		private $docente="";//en base de datos aparece como razon_social
		private $vacante="";
		private $inicio="";
		private $fin="";
		private $horas="";
	
		public function __construct($alumno,$docente,$vacante,$inicio,$fin,$horas){
			$mysqli=conectar();
			$this->alumno=$mysqli->real_escape_string($alumno);
			$this->docente=$mysqli->real_escape_string($docente);
			$this->vacante=$mysqli->real_escape_string($vacante);
			$this->inicio=$mysqli->real_escape_string($inicio);
			$this->fin=$mysqli->real_escape_string($fin);
			$this->horas=$mysqli->real_escape_string($horas);
			desconectar($mysqli);
		}

		public function __get($variable){
			if($variable=="alumno"||$variable=="docente"||$variable=="vacante"||$variable=="inicio"||$variable=="fin"||$variable=="horas"){
				return $this->$variable;
			}
		}

		public function __set($variable,$valor){
			if($variable=="alumno"||$variable=="docente"||$variable=="vacante"||$variable=="inicio"||$variable=="fin"||$variable=="horas"){
				$this->$variable=$valor;
			}
		}

		public function existe(){
			$mysqli=conectar();
			$existe=false;
	        $sql="SELECT * FROM fct";
			//echo ($sql);
			$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}
			while($fila =$resultado->fetch_assoc()){
				if($fila['alumno']==$this->alumno){
					echo "este alumno ya ha realizado fct";
					$existe=true;
				}
			}
			return $existe;
			desconectar($mysqli);
		}

		public function __toString(){
			return "Alumno=$this->slumno, Docente=$this->docente, Vacante=$this->vacante, Inicio=$this->inicio, Fin=$this->fin, Horas=$this->horas";
		}

		public function crear($log){
			$existe=$this->existe();
			if (!$existe) {
				$mysqli=conectar();

				$sql="INSERT INTO `fct` (`alumno`, `docente`, `vacante`, `inicio`, `fin`, `horas`) VALUES ('$this->alumno', '$this->docente', '$this->vacante','$this->inicio', '$this->fin', '$this->horas')";

				$mysqli->query($sql);
				if($mysqli->errno) {
					$log.="Error en consulta\r\n";
				}
				
				desconectar($mysqli);
				
				$log.="registro guardado\r\n";
			}
			else{
				$log.= "esta fct ya existe\r\n";
			}
			return $log;
		}		
		
		public function modificar($id,$log){
			$mysqli=conectar();

			$sql = "UPDATE fct SET alumno = '".$this->alumno."', docente = '".$this->docente."', vacante = '".$this->vacante."', inicio = '".$this->inicio."', fin = '".$this->fin."', horas = '".$this->horas."' where id=$id";
			
			$mysqli->query($sql);  
			if($mysqli->connect_errno){
				$log.="Error en consulta\r\n";
			}

			desconectar($mysqli);
			$log.="Registro guardado\r\n";		
			return $log;
		}
	}
?>
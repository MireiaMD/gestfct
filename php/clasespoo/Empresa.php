<?php
	require_once ('BaseDatos.php');
	
	class Empresa{
		private $id="";
		private $nombre="";
		private $razon="";//en base de datos aparece como razon_social
		private $direccion="";
		private $codpostal="";
		private $poblacion="";
		private $provincia="";
		private $pais="";
		private $tutor="";//en base de datos aparece como tutorempresa
		private $telefono="";
		private $movil="";
		private $mail="";//en base de datos aparece como email
	
		public function __construct($nombre,$razon="",$direccion="",$codpostal="",$poblacion="",$provincia="",$pais="",$tutor="",$telefono,$movil,$mail){
			$mysqli=conectar();
			$this->nombre=$mysqli->real_escape_string($nombre);
			$this->razon=$mysqli->real_escape_string($razon);
			$this->direccion=$mysqli->real_escape_string($direccion);
			$this->codpostal=$mysqli->real_escape_string($codpostal);
			$this->poblacion=$mysqli->real_escape_string($poblacion);
			$this->provincia=$mysqli->real_escape_string($provincia);
			$this->pais=$mysqli->real_escape_string($pais);
			$this->tutor=$mysqli->real_escape_string($tutor);
			$this->telefono=$mysqli->real_escape_string($telefono);
			$this->movil=$mysqli->real_escape_string($movil);
			$this->mail=$mysqli->real_escape_string($mail);
			desconectar($mysqli);
		}

		public function __get($variable){
			if($variable=="nombre"||$variable=="razon"||$variable=="direccion"||$variable=="codpostal"||$variable=="poblacion"||$variable=="provincia"||$variable=="tutor"||$variable=="telefono"||$variable=="movil"||$variable=="mail"){
				return $this->$variable;
			}
		}

		public function __set($variable,$valor){
			if($variable=="nombre"||$variable=="razon"||$variable=="direccion"||$variable=="codpostal"||$variable=="poblacion"||$variable=="provincia"||$variable=="tutor"||$variable=="telefono"||$variable=="movil"||$variable=="mail"){
				$this->$variable=$valor;
			}
		}

		
		public function existe(){
			$mysqli=conectar();
			$existe=false;
	        $sql="SELECT * FROM empresas";
			$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}
			while($fila =$resultado->fetch_assoc()){
				if($fila['razon_social']==$this->razon){
					//echo "esta empresa ya esta registrada";
					$existe=true;
				}
			}
			return $existe;
			desconectar($mysqli);

		}

		public function __toString(){
			return "Nombre=$this->nombre, Razon social=$this->razon, Direccion=$this->direccion, Codigo postal=$this->codpostal, Poblacion=$this->poblacion, Provincia=$this->provincia, Pais=$this->pais,Tutor=$this->tutor, Telefono=$this->telefono, Movil=$this->movil, Correo electronico=$this->mail";
		}

		public function crear($log){
			$existe=$this->existe();
			if (!$existe) {
				$mysqli=conectar();

				$sql="INSERT INTO `empresas` (`nombre`, `razon_social`, `direccion`, `codpostal`, `poblacion`, `provincia`, `pais`, `tutorempresa`, `telefono`, `movil`, `email`) VALUES ('$this->nombre', '$this->razon', '$this->direccion','$this->codpostal', '$this->poblacion', '$this->provincia', '$this->pais', '$this->tutor', '$this->telefono', '$this->movil', '$this->mail')";

				$mysqli->query($sql);
				if($mysqli->errno) {
					$log.="Error en consulta\r\n";
				}
				
				desconectar($mysqli);
				
				$log.= "registro guardado\r\n";
			}
			else{
				$log.= "esta empresa ya existe \r\n";
			}
			return $log;
		}		
		
		public function modificar($id,$log){
			$mysqli=conectar();

			$sql = "UPDATE empresas SET nombre = '".$this->nombre."', razon_social = '".$this->razon."', direccion = '".$this->direccion."', codpostal = '".$this->codpostal."', poblacion = '".$this->poblacion."', provincia = '".$this->provincia."', pais = '".$this->pais."', tutorempresa = '".$this->tutor."', telefono = '".$this->telefono."', movil = '".$this->movil."', email= '".$this->mail."' where id=$id";
			
			$mysqli->query($sql);  
			if($mysqli->connect_errno){
				$log.="Error en consulta\r\n";
			}
			desconectar($mysqli);
			$log.= "Registro guardado\r\n";
			return $log;
		}
	}
?>

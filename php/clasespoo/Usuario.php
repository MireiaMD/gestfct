<?php
	/*
	 *
	 *Clase usuario, clase madre o superclase de Administrador, Tutor, Profesor y Alumno
	 *
	 *Requiere de BaseDatos.php para funcionar.
	 *
	 *Tiene constructor, setter, getter, metodo para generar usuario y contraseña, metodo para regenerar usuario y 
	 *contraseña si fuera necesario, metodo para comprobar si el usuario existe o no en la bd, metodo tostring, metodo
	 *eliminar que pasa el usuario a estado activo=0, comprueba_tipo y obtenid para usar desde las clases hijas, modificarusu 
	 *se llama desde modificar cada usuario para modificar en la tabla usuario.
	 *
	 */
	require_once ('BaseDatos.php');
	class Usuario{
		protected $id="";
		protected $mail="";
		protected $usuario="";
		protected $passwd="";
		protected $tipo="";
		protected $alta="";
		protected $baja="";
		protected $activo="";

		public function __construct($mail,$tipo=""){
			$mysqli=conectar();
			$this->tipo=$mysqli->real_escape_string($tipo);
			$this->mail=$mysqli->real_escape_string($mail);
			$this->regenera();
			desconectar($mysqli);
		}

		public function __get($variable){
			return $this->$variable;
		}

		public function __set($variable,$valor){
			$this->$variable=$valor;
		}

		public function generaPass(){
		    //Defino cadena de caracteres que podran formar parte de la pass.
		    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_-*/abcdefghijklmnopqrstuvwxyz@#!&%/1234567890";
		    //Obtener longitud cadena de caracteres
		    $longitudCadena=strlen($cadena);
		     
		    //Variable que va a contener la contraseña
		    $pass = "";
		    //Longitud de la contraseña.
		    $longitudPass=10;
		     
		    //Crear
		    for($i=1 ; $i<=$longitudPass ; $i++){
		        //Definir una posicion o caracter de la cadena
		        $pos=rand(0,$longitudCadena-1);
		     
		        //Formar la contraseña, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
		        $pass .= substr($cadena,$pos,1);
		    }
		    return $pass;
		}
		/*
		 *
		 *funcion que sustituye los caracteres con tildes para que no vayan en el usuario.
		 *
		 */
		public function quitar_tildes($cadena) {
			$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú");
			$permitidas= array ("a","e","i","o","u","A","E","I","O","U");
			$texto = str_replace($no_permitidas, $permitidas ,$cadena);
			return $texto;
		}

		public function generaUsu(){
			$usu="";
			if($this->tipo==""){
				$this->nombre="";
				$this->apellidos="";
			}
			$nombre=trim($this->nombre);
			$nombre=$this->quitar_tildes($nombre);
			switch ($this->tipo) {
				case 'administrador':
					$usu='ad';
					break;
				case 'tutor':
					$usu='t';
					break;
				case 'profesor':
					$usu='p';
					break;
				case 'alumno':
					$usu='al';
					break;
			}
			$usu.=date("y");
			$usu.=$nombre;
			/*
			 *
			 *Las funciones mb_strtolower o mb_substr sirven para poder usar caracteres como la ñ o ç
			 *
			 */
			$ap=mb_substr ( $this->apellidos , 0 , 2 ,'UTF-8' );
			$ap.=mb_substr ( $this->apellidos , -2, NULL, 'UTF-8' );
			$ap=$this->quitar_tildes($ap);
			$usu.=$ap;
			$usu=mb_strtolower($usu, 'UTF-8');
			return $usu;
		}

		/**
		 *
		 * USAR despues de tener el nombre y los apellidos
		 *
		 */
		public function regenera(){
			$mysqli=conectar();
			$usuario=$this->generaUsu();
			$this->usuario=$mysqli->real_escape_string($usuario);
			$passwd=$this->generaPass();
			$this->passwd=$mysqli->real_escape_string($passwd);
			desconectar($mysqli);
		}
		public function existe(){
			$mysqli=conectar();
			$existe=false;
	        $sql="SELECT * FROM usuarios WHERE usuario='".$this->usuario."' or email='".$this->mail."'";
	        /****CUANDO ESTE TERMINADO USAR LA SQL COMENTADA****/
	        //$sql="SELECT * FROM usuarios WHERE usuario='".$this->usuario."' or email='".$this->mail."' or dni='".$this->dni."'";
			$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}

			if($resultado->num_rows>0){
				$existe=true;
			}
			return $existe;
			desconectar($mysqli);
		}

		public function comprueba_tipo(){
			switch ($this->tipo) {
				case 'administrador':
					return 1;
					break;
				case 'tutor':
					return 2;
					break;
				case 'profesor':
					return 3;
					break;
				case 'alumno':
					return 4;
					break;
			}
		}

		public function __toString(){
			return "Correo electronico=$this->mail, Usuario=$this->usuario, Contraseña=$this->passwd, Tipo=$this->tipo, Fecha de alta=$this->alta,Fecha de baja=$this->baja,Activo=$this->activo";
		}

		public function eliminar($id){
			$this->baja = date("Y-m-d H:i:s");
            $this->activo = 0;

            $mysqli=conectar();

            $sql = "UPDATE usuarios SET baja = now(), activo = '$this->activo' where id=$id";
            
            $mysqli->query($sql);  
			if($mysqli->errno){
				return("Error en consulta");
			}

			desconectar($mysqli);
			echo "Registro borrado";
		}

		public function modificarusu($id){
			$mysqli=conectar();

			$sql = "UPDATE usuarios SET email = '".$this->mail."' where id=$id";
			
			$mysqli->query($sql);  
			if($mysqli->connect_errno){
				return("Error en consulta");
			}

			desconectar($mysqli);	
		}
		/*
		 *
		 *Funcion para enviar correos en codificación utf-8, de esta forma admite tildes, ñ o ç
		 *
		 */
		public function mail_utf8($para, $asunto = '(Sin asunto)', $cuerpo = '', $cabe = '',$log) {
			$cabeini='MIME-Version: 1.0'."\r\n".'Content-type: text/plain; charset=UTF-8'."\r\n";
			if(mail($para, '=?UTF-8?B?'.base64_encode($asunto).'?=', $cuerpo, $cabeini . $cabe)){
				$log.= "Correo enviado con éxito\r\n";
			}
			else{
				$log.= "Error en el envío\r\n";
			}
			return $log;
		}
	}

?>

<?php
	require_once ('Usuario.php');

	class Alumno extends Usuario{
		private $nombre="";
		private $apellidos="";
		private $dni="";
		private $fechanac="";
		private $direccion="";
		private $codpostal="";
		private $poblacion="";
		private $provincia="";
		private $titulo="";
		private $telefono="";
		private $estudiosant="";
		private $experiencia="";
		private $preferencias="";
		private $tutor="";

		public function __construct($mail,$nombre,$apellidos,$dni="",$fechanac="",$direccion="",$codpostal="",$poblacion="",$provincia="",$titulo="",$estudiosant="",$experiencia="",$preferencias="",$tutor="",$telefono=""){
			$mysqli=conectar();
			$this->nombre=$mysqli->real_escape_string($nombre);
			$this->apellidos=$mysqli->real_escape_string($apellidos);
			parent::__construct($mail,"alumno");
			$this->dni=$mysqli->real_escape_string($dni);
			$this->fechanac=$mysqli->real_escape_string($fechanac);
			$this->direccion=$mysqli->real_escape_string($direccion);
			$this->codpostal=$mysqli->real_escape_string($codpostal);
			$this->poblacion=$mysqli->real_escape_string($poblacion);
			$this->provincia=$mysqli->real_escape_string($provincia);
			$this->titulo=$mysqli->real_escape_string($titulo);
			$this->estudiosant=$mysqli->real_escape_string($estudiosant);
			$this->experiencia=$mysqli->real_escape_string($experiencia);
			$this->preferencias=$mysqli->real_escape_string($preferencias);
			$this->tutor=$mysqli->real_escape_string($tutor);
			$this->telefono=$mysqli->real_escape_string($telefono);
			desconectar($mysqli);
		}

		public function __get($variable){
			if($variable=="nombre"||$variable=="apellidos"||$variable=="dni"||$variable=="fechanac"||$variable=="direccion"||$variable=="codpostal"||$variable=="poblacion"||$variable=="provincia"||$variable=="titulo"||$variable=="estudiosant"||$variable=="experiencia"||$variable=="preferencias"||$variable=="tutor"){
				return $this->$variable;
			}
			else{
				return parent::__get($variable);
			}
		}

		public function __set($variable,$valor){
			if($variable=="nombre"||$variable=="apellidos"||$variable=="dni"||$variable=="fechanac"||$variable=="direccion"||$variable=="codpostal"||$variable=="poblacion"||$variable=="provincia"||$variable=="titulo"||$variable=="estudiosant"||$variable=="experiencia"||$variable=="preferencias"||$variable=="tutor"||$variable=="telefono"){
				$this->$variable=$valor;
			}
			else{
				parent::__set($variable,$valor);
			}
		}

		public function __toString(){
			return (parent::__toString()."Nombre=$this->nombre, Apellidos=$this->apellidos, DNI=$this->dni, Fecha de nacimiento=$this->fechanac, Direccion=$this->direccion, Codigo postal=$this->codpostal, Poblacion=$this->poblacion, Provincia=$this->provincia, Titulo=$this->titulo, Estudios anteriores=$this->estudiosant, Experiencia=$this->experiencia, Preferencias=$this->preferencias, Tutor=$this->tutor, Telefono=$this->telefono<br/>");
		}

		public function crear($log){
			$existe=$this->existe();
			$tipo=$this->comprueba_tipo();
			if (!$existe) {
				$this->alta = date("Y-m-d H:i:s");
				$this->activo=1;

				$hash=password_hash($this->passwd,PASSWORD_DEFAULT);

				$mysqli=conectar();

				$mysqli->autocommit(false);
				$bandera = true;

				$sql="INSERT INTO `usuarios` (`email`, `usuario`, `passwd`, `tipousuario`) VALUES ('$this->mail', '$this->usuario', '$hash', '$tipo')";
				
				$mysqli->query($sql);  
				if($mysqli->errno) {
					$log.="Error en la consulta 1\r\n";
					$bandera=false;
				}
				
				$this->id=$mysqli->insert_id;

				$sql="INSERT INTO `alumnos` (`id`, `nombre`, `apellidos`, `dni`, `fechanac`, `direccion`, `codpostal`, `poblacion`, `provincia`, `titulo`, `estudiosant`, `experiencia`, `preferencias`, `idtutor`, `telefono`) VALUES ('$this->id', '$this->nombre', '$this->apellidos', '$this->dni','$this->fechanac', '$this->direccion', '$this->codpostal', '$this->poblacion', '$this->provincia', '$this->titulo', '$this->estudiosant', '$this->experiencia', '$this->preferencias', '$this->tutor', '$this->telefono')";

				$mysqli->query($sql);
				if($mysqli->errno) {
					$log.="Error en consulta 2\r\n";
					$bandera=false;
				}
				if ($bandera) {  
					$mysqli->commit();
					$log.=" registro guardado\r\n";
					$para=$this->mail;
					$cuerpo = "Bienvenido a GestFCT ".$this->nombre." ".$this->apellidos.",\r\n\r\nTu usuario es: ".$this->usuario.", y tu contraseña es: ".$this->passwd.".\r\n\r\nGracias por registrarte";
					$asunto = "Registro en GestFCT";
					$cabe='';
					$log=$this->mail_utf8($para, $asunto, $cuerpo, $cabe,$log);

				} else {  
					$mysqli->rollback();  
					$log.="Todas las consultas han sido revertidas\r\n";  
				}  		
				
				desconectar($mysqli);
			}
			else{
				$log.="este usuario ya existe\r\n";
			}
			return $log;
		}		
		
		public function modificar($id,$log){
			$mysqli=conectar();

			$sql = "UPDATE alumnos SET nombre = '".$this->nombre."', apellidos = '".$this->apellidos."', dni = '".$this->dni."', fechanac = '".$this->fechanac."', direccion = '".$this->direccion."', codpostal = '".$this->codpostal."', poblacion = '".$this->poblacion."', provincia = '".$this->provincia."', telefono = '".$this->telefono."', titulo = '".$this->titulo."', estudiosant = '".$this->estudiosant."', experiencia = '".$this->experiencia."', preferencias = '".$this->preferencias."' where id=$id";
			
			$mysqli->query($sql);  
			if($mysqli->connect_errno){
				$log.="Error en consulta\r\n";
			}

			desconectar($mysqli);
			$this->modificarusu($id);
			$log.="Registro guardado\r\n";
			return $log;		
		}
	}
?>
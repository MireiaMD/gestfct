<?php
	require_once ('BaseDatos.php');
	
	class Fcthoras{
		private $id="";
		private $fct="";
		private $fecha="";
		private $numhoras="";
		private $obs="";

		public function __construct($fct,$fecha,$numhoras,$obs){
			$mysqli=conectar();
			$this->fct=$mysqli->real_escape_string($fct);
			$this->fecha=$mysqli->real_escape_string($fecha);
			$this->numhoras=$mysqli->real_escape_string($numhoras);
			$this->obs=$mysqli->real_escape_string($obs);
			desconectar($mysqli);
		}

		public function __get($variable){
			if($variable=="fct"||$variable=="fecha"||$variable=="numhoras"||$variable=="obs"){
				return $this->$variable;
			}
		}

		public function __set($variable,$valor){
			if($variable=="fct"||$variable=="fecha"||$variable=="numhoras"||$variable=="obs"){
				$this->$variable=$valor;
			}
		}

		public function existe(){
			$mysqli=conectar();
			$existe=false;
	        $sql="SELECT * FROM fcthoras";
			$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}
			while($fila =$resultado->fetch_assoc()){
				$ano=substr($fila['fecha'],0,4);
				$mes=substr($fila['fecha'],5,2);
				$dia=substr($fila['fecha'],8,2);
				$fecha=$dia."-".$mes."-".$ano;
				if($fecha==$this->fecha){
					echo "esta fecha ya esta registrada";
					$existe=true;
				}
			}
			return $existe;
			desconectar($mysqli);
		}
		
		public function __toString(){
			return "fct=$this->fct, fecha=$this->fecha, numhoras=$this->numhoras, observaciones=$this->obs";
		}

		public function crear($log){
			$existe=$this->existe();
			if (!$existe) {
				$mysqli=conectar();

				$sql="INSERT INTO `fcthoras` (`idfct`, `fecha`, `numhoras`, `observaciones`) VALUES ('$this->fct', '$this->fecha', '$this->numhoras', '$this->obs')";
				$mysqli->query($sql);
				if($mysqli->errno) {
					$log.="Error en consulta\r\n";
				}
				
				desconectar($mysqli);
				
				$log.= "registro guardado\r\n";
			}
			else{
				$log.= "este dia ya existe\r\n";
			}
			return $log;
		}		
		
		public function modificar($id,$log){
			$mysqli=conectar();
			$sql = "UPDATE fcthoras SET idfct = '".$this->fct."', fecha = '".$this->fecha."', numhoras = '".$this->numhoras."', observaciones = '".$this->obs."' where id='$this->id'";
			
			$mysqli->query($sql);  
			if($mysqli->connect_errno){
				$log.= "Error en consulta\r\n";
			}

			desconectar($mysqli);
			$log.=  "Registro guardado\r\n";		
			return $log;
		}
	}
?>
<?php
	require_once ('Usuario.php');

	class Tutor extends Usuario{
		private $nombre="";
		private $apellidos="";
		private $dni="";

		public function __construct($mail,$nombre,$apellidos,$dni=""){
			$mysqli=conectar();
			$this->nombre=$mysqli->real_escape_string($nombre);
			$this->apellidos=$mysqli->real_escape_string($apellidos);
			parent::__construct($mail,"tutor");
			$this->dni=$mysqli->real_escape_string($dni);
			desconectar($mysqli);
		}

		public function __get($variable){
			if($variable=="nombre"||$variable=="apellidos"||$variable=="dni"){
				return $this->$variable;
			}
			else{
				return parent::__get($variable);
			}
		}

		public function __set($variable,$valor){
			if($variable=="nombre"||$variable=="apellidos"||$variable=="dni"){
				$this->$variable=$valor;
			}
			else{
				parent::__set($variable,$valor);
			}
		}

		public function __toString(){
			return (parent::__toString()."Nombre=$this->nombre, Apellidos=$this->apellidos, DNI=$this->dni<br/>");
		}

		public function crear($log){
			$existe=$this->existe();
			$tipo=$this->comprueba_tipo();
			if (!$existe) {
				$this->alta = date("Y-m-d H:i:s");
				$this->activo=1;

				$hash=password_hash($this->passwd,PASSWORD_DEFAULT);

				$mysqli=conectar();

				$mysqli->autocommit(false);
				$bandera = true;

				$sql="INSERT INTO `usuarios` (`email`, `usuario`, `passwd`, `tipousuario`) VALUES ('$this->mail', '$this->usuario', '$hash', '$tipo')";
				
				$mysqli->query($sql);  
				if($mysqli->errno) {
					$log.="Error en la consulta 1\r\n";
					$bandera=false;
				}
				
				$this->id=$mysqli->insert_id;

				$sql="INSERT INTO `docentes` (`id`, `nombre`, `apellidos`, `dni`) VALUES ('$this->id', '$this->nombre', '$this->apellidos', '$this->dni')"; 
				$mysqli->query($sql);
				if($mysqli->errno) {
					$log.="Error en la consulta 2\r\n";
					$bandera=false;
				}
				if ($bandera) {  
					$mysqli->commit();
					$log.= "registro guardado\r\n";
					$para=$this->mail;
					$cuerpo = "Bienvenido a GestFCT ".$this->nombre." ".$this->apellidos.",\r\n\r\nTu usuario es: ".$this->usuario.", y tu contraseña es: ".$this->passwd.".\r\n\r\nGracias por registrarte";
					$asunto = "Registro en GestFCT";
					$cabe='';
					$log=$this->mail_utf8($para, $asunto, $cuerpo, $cabe, $log);

				} 
				else {  
					$mysqli->rollback();  
					$log.= "Todas las consultas han sido revertidas\r\n";  
				}  		
				
				desconectar($mysqli);
			}
			else{
				$log.= "este usuario ya existe\r\n";
			}
			return $log;
		}		
		
		public function modificar($id,$log){
			$mysqli=conectar();

			$sql = "UPDATE docentes SET nombre = '".$this->nombre."', apellidos = '".$this->apellidos."', dni = '".$this->dni."' where id=$id";
			
			$mysqli->query($sql);  
			if($mysqli->connect_errno){
				$log.="Error en consulta\r\n";
			}

			desconectar($mysqli);
			$this->modificarusu($id);
			$log.= "Registro guardado\r\n";
			return $log;	
		}
	}
?>
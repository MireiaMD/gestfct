<?php
	function comprmail($email){
		if(!$email==""){
			if(strlen($email)<=60){
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
					return true;
				}
			}
			else{
				return false;
				echo "Correo no válido";
			}
		}
		else{
			return false;
			echo "debe indicar un email";
		}
	}

	function comprnombre($nombre){
		if(!$nombre==""){
			if(strlen( $nombre )<=45){
				return true;
			}
			else{
				return false;
				echo "Supera el tamaño válido";
			}
		}
		else{
			return false;
			echo "debe indicar un nombre";
		}
	}

	function comprapell($apellidos){
		if(!$apellidos==""){
			if(strlen( $apellidos )<=45){
				return true;
			}
			else{
				return false;
				echo "Supera el tamaño válido";
			}
		}
		else{
			return false;
			echo "debe indicar apellidos";
		}
	}

	function comprdni($dni){
		if(!$dni==""){
			if(strlen($dni)<9) {
				return false;
				echo "DNI demasiado corto.";
			}
			else{
				$dni = strtoupper($dni);

				$letra = substr($dni, -1, 1);
				$numero = substr($dni, 0, 8);

				// Si es un NIE hay que cambiar la primera letra por 0, 1 ó 2 dependiendo de si es X, Y o Z.
				$numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);	

				$modulo = $numero % 23;
				$letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
				$letra_correcta = substr($letras_validas, $modulo, 1);

				if($letra_correcta!=$letra) {
					echo "DNI o NIE incorrecto.";
					return false;
				}
				else {
					return true;
				}
			}	
		}
		else{
			return false;
			echo "debe indicar un dni";
		}
	}

	function comprdir($direccion){
		if(strlen( $direccion )<=60){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprpobl($poblacion){
		if(strlen( $poblacion )<=45){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprprov($provincia){
		if(strlen( $provincia )<=45){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}		
	}

	function comprpais($pais){
		if(strlen( $pais )<=45){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprcp($codpostal){
		if(strlen( $codpostal )<=5){
			if(is_numeric($codpostal)){
				return true;
			}
			else{
				return false;
				echo "No es numerico";
			}
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprcpemp($codpostal){
		if(strlen( $codpostal )<=5){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprestant($estudiosant){
		if(strlen( $estudiosant )<=30){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprtitulo($titulo){
		if(strlen( $titulo )<=30){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprexp($experiencia){
		if(strlen( $experiencia )<=60){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprpref($preferencias){
		if(strlen( $preferencias )<=60){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function compridtutor($idtutor){
		if(is_numeric($idtutor)){
			return true;
		}
		else{
			return false;
			echo "No es una id de tutor válida";
		}
	}

	function comprnombremp($nombre){
		if(!$nombre==""){
			if(strlen( $nombre )<=50){
				return true;
			}
			else{
				return false;
				echo "Supera el tamaño válido";
			}
		}
		else{
			return false;
			echo "debe indicar un nombre de empresa";
		}
	}	

	function comprrazon($razon){
		if(strlen( $razon )<=100){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprtutoremp($tutoremp){
		if(!$tutoremp==""){
			if(strlen( $tutoremp )<=20){
				return true;
			}
			else{
				return false;
				echo "Supera el tamaño válido";
			}
		}
		else{
			return false;
			echo "debe indicar un tutor en la empresa";
		}
	}

	function comprtelefono($telefono){
		if(!$telefono==""){
			if(strlen( $telefono )<=9){
				return true;
			}
			else{
				return false;
				echo "Supera el tamaño válido";
			}
		}
		else{
			return false;
			echo "debe indicar un telefono";
		}
	}

	function comprtelefonoempr($telefono){
		if(!$telefono==""){
			if(strlen( $telefono )<=20){
				return true;
			}
			else{
				return false;
				echo "Supera el tamaño válido";
			}
		}
		else{
			return false;
			echo "debe indicar un telefono";
		}
	}


	function comprmovil($movil){
		if(!$movil==""){
			if(strlen( $movil )<=20){
				return true;
			}
			else{
				return false;
				echo "Supera el tamaño válido";
			}
		}
		else{
			return false;
			echo "debe indicar un movil";
		}
	}

	function compridempr($idempr){
		if(is_numeric($idempr)){
			return true;
		}
		else{
			return false;
			echo "No es una id de empresa válida";
		}
	}

	function comprrequisitostec($requisitostec){
		if(strlen( $requisitostec )<=60){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprcurso($curso){
		if(strlen( $curso )<=4){
			if(is_numeric($curso)){
				return true;
			}
			else{
				echo "no es un dato numerico";
			}
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function compridalum($idalum){
		if(is_numeric($idalum)){
			return true;
		}
		else{
			return false;
			echo "No es una id de empresa válida";
		}
	}

	function compridvac($idvac){
		if(is_numeric($idvac)){
			return true;
		}
		else{
			return false;
			echo "No es una id de empresa válida";
		}
	}

	function comprhoras($horas){
		if(strlen( $horas )<=3){
			if(is_numeric($idempr)){
				return true;
			}
			else{
				echo "no es un dato numerico";
			}
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function compridfct($idfct){
		if(is_numeric($idfct)){
			return true;
		}
		else{
			return false;
			echo "No es una id de fct válida";
		}
	}	

	function compridfcth($idfcth){
		if(is_numeric($idfcth)){
			return true;
		}
		else{
			return false;
			echo "No es una id de fct diario válida";
		}
	}	

	function comprnumhoras($numhoras){
		if(strlen( $numhoras )<=2){
			if(is_numeric($numhoras)){
				if($numhoras<=10){
					return true;
				}
				else{
					echo "numero de horas muy alto";
				}
			}
			else{
				echo "no es un dato numerico";
			}
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprobs($observaciones){
		if(strlen( $observaciones )<=60){
			return true;
		}
		else{
			return false;
			echo "Supera el tamaño válido";
		}
	}

	function comprfecha($fecha){
		$valores = explode('-', $fecha);
		if(count($valores) == 3 && checkdate($valores[1], $valores[2], $valores[0])){
			return true;
	    }
		return false;
		echo "no es una fecha válida";
	}

	function comprfechad($fecha,$inicio,$fin){
		$valores = explode('-', $fecha);
		if(count($valores) == 3 && checkdate($valores[1], $valores[2], $valores[0])){
			if($inicio<$fecha && $fecha<$fin){
				return true;
			}
	    }
		return false;
		echo "no es una fecha válida";
	}
?>

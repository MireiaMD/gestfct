<?php
	$opc=$_GET['opc'];
	switch ($opc) {
		case '1':
			require_once('BaseDatos.php');
			header('Content-Type:aplication/json; charset=utf-8');
			
			$mysqli=conectar();
			$sql="SELECT email,usuarios.id,usuario,tipo,alta FROM usuarios inner join tiposusuario on usuarios.tipousuario=tiposusuario.id WHERE activo=1";
			$result=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$resulta['data']=array();
			while($registros =$result->fetch_assoc()){
				$resulta['data'][]=$registros;	
			}
			desconectar($mysqli);
			print_r(json_encode($resulta));
			break;
		case '2':
			require_once('BaseDatos.php');
			header('Content-Type:aplication/json; charset=utf-8');
			
			$mysqli=conectar();
			$sql="SELECT email,usuarios.id,usuario,tipo,baja FROM usuarios inner join tiposusuario on usuarios.tipousuario=tiposusuario.id WHERE activo=0";
			$result=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$resulta['data']=array();
			while($registros =$result->fetch_assoc()){
				$resulta['data'][]=$registros;	
			}
			desconectar($mysqli);
			print_r(json_encode($resulta));
			break;
		case '3':
			require_once('BaseDatos.php');
			header('Content-Type:aplication/json; charset=utf-8');
			
			$mysqli=conectar();
			$sql="SELECT * FROM docentes INNER JOIN usuarios ON docentes.id=usuarios.id WHERE tipousuario=2 and activo=1";
			$result=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$resulta['data']=array();
			while($registros =$result->fetch_assoc()){
				$resulta['data'][]=$registros;	
			}
			desconectar($mysqli);
			print_r(json_encode($resulta));
			break;
		case '4':
			require_once('BaseDatos.php');
			header('Content-Type:aplication/json; charset=utf-8');
			
			$mysqli=conectar();
			$sql="SELECT * FROM docentes INNER JOIN usuarios ON docentes.id=usuarios.id WHERE tipousuario=3 and activo=1";
			$result=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$resulta['data']=array();
			while($registros =$result->fetch_assoc()){
				$resulta['data'][]=$registros;	
			}
			desconectar($mysqli);

			print_r(json_encode($resulta));
			break;
		case '5':
			session_start();
			if($_SESSION['tipousuario']==1){
				require_once('BaseDatos.php');
				header('Content-Type:aplication/json; charset=utf-8');
				
				$mysqli=conectar();

				$sql="SELECT * FROM alumnos INNER JOIN usuarios ON alumnos.id=usuarios.id WHERE tipousuario=4 and activo=1";
				$result=$mysqli->query($sql);
				if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
				$resulta['data']=array();
				while($registros =$result->fetch_assoc()){
					$resulta['data'][]=$registros;	
				}
			}
			else if($_SESSION['tipousuario']==2){
				$idtutor=$_SESSION['id'];
				require_once('BaseDatos.php');
				header('Content-Type:aplication/json; charset=utf-8');
				
				$mysqli=conectar();

				$sql="SELECT * FROM alumnos INNER JOIN usuarios ON alumnos.id=usuarios.id WHERE tipousuario=4 and activo=1 and idtutor='$idtutor'";
				$result=$mysqli->query($sql);
				if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
				$resulta['data']=array();
				while($registros =$result->fetch_assoc()){
					$resulta['data'][]=$registros;	
				}
			}
			desconectar($mysqli);
			print_r(json_encode($resulta));
			break;
		case'6':
			require_once('BaseDatos.php');
			header('Content-Type:aplication/json; charset=utf-8');
			
			$mysqli=conectar();
			$sql="SELECT * FROM empresas";
			$result=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$resulta['data']=array();
			while($registros =$result->fetch_assoc()){
				$resulta['data'][]=$registros;	
			}
			desconectar($mysqli);
			print_r(json_encode($resulta));
			break;
		case '7':
			require_once('BaseDatos.php');
			header('Content-Type:aplication/json; charset=utf-8');
			$mysqli=conectar();
			$sql="SELECT `fct`.`id`,`alumnos`.`nombre` as nomalumno,`alumnos`.`apellidos` as apellalumno,`docentes`.`nombre` as nomdocente,`docentes`.`apellidos` as apelldocente,inicio,fin,horas,vacante FROM fct inner join usuarios on alumno=usuarios.id inner join alumnos on alumno=alumnos.id inner join docentes on docente=docentes.id where usuarios.activo=1";
			$resultado=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$resulta['data']=array();
			while($fila=$resultado->fetch_assoc()){
				$inicio=$fila['inicio'];
				$fin=$fila['fin'];
				$horas=$fila['horas'];
				$id=$fila['id'];
				$idvacante=$fila['vacante'];
				$alumno=$fila['nomalumno']." ".$fila['apellalumno'];
				$docente=$fila['nomdocente']." ".$fila['apelldocente'];
				$sql="SELECT `nombre`,`requisitostec` FROM vacantes inner join empresas on empresas.id=idempresa where vacantes.id=$idvacante";
				$resultadob=$mysqli->query($sql);
				if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
				$filab=$resultadob->fetch_assoc();
				$vacante="NOMBRE: ".$filab['nombre']."<br/>REQUISITOS: ".$filab['requisitostec'];
				$registro= ["id" => $id, "alumno" => $alumno, "docente" => $docente, "inicio" => $inicio, "fin" => $fin, "horas" => $horas, "vacante" => $vacante];
				$resulta['data'][]=$registro;
			}
			desconectar($mysqli);
			
			print_r(json_encode($resulta));
			break;
		case '8';
			require_once('BaseDatos.php');
			session_start();
			header('Content-Type:aplication/json; charset=utf-8');
			$idfct=$_SESSION['idfct'];
			$mysqli=conectar();
			$sql="SELECT * FROM fcthoras WHERE idfct='$idfct'";
			$result=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$resulta['data']=array();
			while($registros =$result->fetch_assoc()){
				$resulta['data'][]=$registros;	
			}
			desconectar($mysqli);

			print_r(json_encode($resulta));
			break;
	}
?>
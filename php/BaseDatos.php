<?php
	/*
	 *
	 *Fichero que tiene dos funciones, conectar y desconectar
	 *Crean o destruyen la conexion a la base de datos
	 *Seran utilizado a lo largo del proyecto desde otros ficheros y desde las clases
	 *
	 */
	require_once ('config.php');

	function conectar(){
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME); 
							
		if( $mysqli->connect_errno ){ 
	            echo "Fallo de conexion"; 
	            return;     
	       } 

        $mysqli->query("set names '".DB_CHARSET."'");
        return $mysqli;	
	}
	
	function desconectar($mysqli){
		$mysqli->close();
	}
?>

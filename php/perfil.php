<?php
	require_once ('pintar/pintar.php');
	
	session_start();

	$usuario=$_SESSION['usuario'];
	$mysqli=conectar();

	$sql="SELECT * FROM usuarios WHERE usuario='$usuario'";
	$resultado = $mysqli->query($sql);
	$filaa = $resultado->fetch_assoc();
	$id=$filaa['id'];
	$usuario=$filaa['usuario'];
	$usuario2='<p><span class="etiqueta">Usuario:&nbsp;</span>'.$filaa['usuario'].'</p>';
	$email='<p><span class="etiqueta">Correo electronico:&nbsp;</span>'.$filaa['email'].'</p>';
	$alta='<p><span class="etiqueta">Fecha de alta:&nbsp;</span>'.$filaa['alta'].'</p>';
	$tipo=$filaa['tipousuario'];
	 
	switch ($tipo) {
		case '1':
			$tabla="administradores";
			break;

		case '2':
			$tabla="docentes";
			break;

		case '3':
			$tabla="docentes";
			break;

		case '4':
			$tabla="alumnos";
			break;
	}
	$sql="SELECT * FROM ".$tabla." WHERE id = '$id'";
	$resultado = $mysqli->query($sql);
	$filab = $resultado->fetch_assoc();
	$nombre='<p><span class="etiqueta">Nombre:&nbsp;</span>'.$filab['nombre'].'</p>';
	$apellidos='<p><span class="etiqueta">Apellidos:&nbsp;</span>'.$filab['apellidos'].'</p>';
	
	if($tipo!=1){
		$dni='<p><span class="etiqueta">DNI:&nbsp;</span>'.$filab['dni'].'</p>';
	}
	
	if($tipo==4){
		$idtutor=$filab['idtutor'];
		$sql="SELECT * FROM docentes WHERE id = '$idtutor'";
		$resultado = $mysqli->query($sql);
		$filac = $resultado->fetch_assoc();
		$tutor=$filac['nombre']." ".$filac['apellidos'];
		$fechanac='<p><span class="etiqueta">Fecha de nacimiento:&nbsp;</span>'.$filab['fechanac'].'</p>';
		$titulo='<p><span class="etiqueta">Titulo:&nbsp;</span>'.$filab['titulo'].'</p>';
		$telefono='<p><span class="etiqueta">Telefono:&nbsp;</span>'.$filab['telefono'].'</p>';
		$direccion='<p><span class="etiqueta">Direccion:&nbsp;</span>'.$filab['direccion'].'</p>';
		$codpostal='<p><span class="etiqueta">Codigo postal:&nbsp;</span>'.$filab['codpostal'].'</p>';
		$poblacion='<p><span class="etiqueta">Poblacion:&nbsp;</span>'.$filab['poblacion'].'</p>';
		$provincia='<p><span class="etiqueta">Provincia:&nbsp;</span>'.$filab['provincia'].'</p>';
		$estudiosant='<p><span class="etiqueta">Estudios anteriores:&nbsp;</span>'.$filab['estudiosant'].'</p>';
		$experiencia='<p><span class="etiqueta">Experiencia:&nbsp;</span>'.$filab['experiencia'].'</p>';
		$preferencias='<p><span class="etiqueta">Preferencias:&nbsp;</span>'.$filab['preferencias'].'</p>';
	}

	$panelTitulo='<div class="panel-title"><h3>Perfil completo de: <span><i>'.$usuario.'</i></span></h3></div>';
	$panelCabe='<div class="panel-heading">'.$panelTitulo.'</div>';
	$panelBody='<div class="panel-body"><div class="row"><div class="col-xs-6">'.$nombre.$apellidos.$email.$alta.'';
	if($tipo==3||$tipo==2){
		$panelBody.=$dni.'</div></div></div>';
		$modal='';
	}
	else if($tipo==4){
		$panelBody.=$dni.$fechanac.$direccion.$codpostal.$poblacion.$provincia.'</div><div class=col-xs-6>'.$telefono.$titulo.$estudiosant.$experiencia.$preferencias.'</div></div></div>';
		$modal='';
	}
	else{
		$panelBody.='</div></div></div>';
		$modal='';
	}
	$panelFooter='<div class="panel-footer">
<button type="submit" class="btn btn-primary btn-sm" id="datos" name="datos">Modificar datos</button>
<button type="submit" class="btn btn-primary btn-sm" id="passwd" name="passwd">Modificar contraseña</button>
</div>';
	$panel='<div class="panel panel-primary">'.$panelCabe.$panelBody.$panelFooter.'</div>';

	$var="perfiljs";
	pintapanel($panel);
	pintaperfil_modal();
	pintascript($var);
	pintafin();
?>

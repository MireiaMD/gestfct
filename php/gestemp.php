<?php
	require_once('comprobar.php');
	$opc=$_POST['opc'];
	switch ($opc) {
		/*añadir empresa*/
		case '1':
			require_once('clasespoo/Empresa.php');
	
			$nombre=$_POST['nombre'];
			$razon_social=$_POST['razon_social'];
			$direccion=$_POST['direccion'];
			$codpostal=$_POST['codpostal'];
			$poblacion=$_POST['poblacion'];
			$provincia=$_POST['provincia'];
			$pais=$_POST['pais'];
			$tutorempresa=$_POST['tutorempresa'];
			$email=$_POST['email'];
			$telefono=$_POST['telefono'];
			$movil=$_POST['movil'];
			$log="";
			$bandera=true;
			if(!comprnombremp($nombre)){
				$log.="nombre demasiado largo\r\n";
				$bandera=false;
			}
			if(!comprrazon($razon_social)){
				$log.="razon social demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprdir($direccion)){
				$log.="direccion demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprcpemp($codpostal)){
				$log.="codigo postal incorrecto\r\n";
				$bandera=false;
			}
			if(!comprpobl($poblacion)){
				$log.="poblacion demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprprov($provincia)){
				$log.="provincia demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprpais($pais)){
				$log.="pais demasiado largo\r\n";
				$bandera=false;
			}
			if(!comprtutoremp($tutorempresa)){
				$log.="nombre del tutor en empresa demasiado largo\r\n";
				$bandera=false;
			}
			if(!comprmail($email)){
				$log.="email incorrecto\r\n";
				$bandera=false;
			}
			if(!comprtelefonoempr($telefono)){
				$log.="telefono incorrecto\r\n";
				$bandera=false;
			}
			if(!comprmovil($movil)){
				$log.="movil incorrecto\r\n";
				$bandera=false;
			}
			if ($bandera){
				$empresa=new Empresa($nombre,$razon_social,$direccion,$codpostal,$poblacion,$provincia,$pais,$tutorempresa,$telefono,$movil,$email);
				$log=$empresa->crear($log);	
			}
			echo $log;
			break;
		/*rellenar formu para editar empresa*/
		case'2':
			require_once('BaseDatos.php');
			$id=$_POST['id'];

			$mysqli=conectar();
			
			$sql="SELECT * FROM empresas WHERE id='$id'";
			$result=$mysqli->query($sql);
			if($mysqli->errno){die('Esto va mal' .$mysqli->error);}
			$registro =$result->fetch_assoc();

			desconectar($mysqli);

			echo (json_encode($registro));
			break;
		/*editar empresa*/
		case '3':
			require_once('clasespoo/Empresa.php');

			$id=$_POST['clave'];
		    $nombre=$_POST['nombre'];
			$razon_social=$_POST['razon_social'];
			$direccion=$_POST['direccion'];
			$codpostal=$_POST['codpostal'];
			$poblacion=$_POST['poblacion'];
			$provincia=$_POST['provincia'];
			$pais=$_POST['pais'];
			$tutorempresa=$_POST['tutorempresa'];
			$email=$_POST['email'];
			$telefono=$_POST['telefono'];
			$movil=$_POST['movil'];
			$log="";
			$bandera=true;
			if(!comprnombremp($nombre)){
				$log.="nombre demasiado largo\r\n";
				$bandera=false;
			}
			if(!comprrazon($razon_social)){
				$log.="razon social demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprdir($direccion)){
				$log.="direccion demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprcpemp($codpostal)){
				$log.="codigo postal incorrecto\r\n";
				$bandera=false;
			}
			if(!comprpobl($poblacion)){
				$log.="poblacion demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprprov($provincia)){
				$log.="provincia demasiado larga\r\n";
				$bandera=false;
			}
			if(!comprpais($pais)){
				$log.="pais demasiado largo\r\n";
				$bandera=false;
			}
			if(!comprtutoremp($tutorempresa)){
				$log.="nombre del tutor en empresa demasiado largo\r\n";
				$bandera=false;
			}
			if(!comprmail($email)){
				$log.="email incorrecto\r\n";
				$bandera=false;
			}
			if(!comprtelefonoempr($telefono)){
				$log.="telefono incorrecto\r\n";
				$bandera=false;
			}
			if(!comprmovil($movil)){
				$log.="movil incorrecto\r\n";
				$bandera=false;
			}
			if ($bandera){
				$empresa=new Empresa($nombre,$razon_social,$direccion,$codpostal,$poblacion,$provincia,$pais,$tutorempresa,$telefono,$movil,$email);
				$log=$empresa->modificar($id,$log);	
			}
			echo $log;
			break;
		/*insertar vacante de empresa*/
		case '4';
			require_once('clasespoo/Vacante.php');

			$idempresa=$_POST['idempresa'];
			$requisitostec=$_POST['requisitostec'];
			$curso_escolar=$_POST['curso_escolar'];
			$log="";
			if(compridempr($idempresa)){
				if(comprrequisitostec($requisitostec)){
					if(comprcurso($curso_escolar)){
						$empresa=new Vacante($idempresa,$requisitostec,$curso_escolar);
						$log=$empresa->crear($log);
					}else{
						$log.="curso incorrecto\r\n";
					}
				}else{
					$log.="requisitos demasiado largo\r\n";
				}
			}else{
				$log.="id empresa incorrecto\r\n";
			}
			echo $log;
			break;
		/*mostrar detalle empresa*/
		case '5':
			require_once('BaseDatos.php');
			
			if( isset($_POST['id']) ){
				$mysqli=conectar();
				
				$id=$_POST['id'];
				
				$pre1="<th>ID</th>
		              <th>Razon social</th>
		              <th>Direccion</th>
		              <th>Codigo postal</th>
		              <th>Poblacion</th>
		              <th>Provincia</th>
		              <th>Pais</th>
		              <th>Vacantes actuales</th>";
		              
				$sql="SELECT * FROM `empresas` WHERE id = '$id'";
				$resultado = $mysqli->query($sql);
				$fila = $resultado->fetch_assoc();
				$sql="SELECT * FROM vacantes WHERE idempresa = '$id' and (curso_escolar like '%2017%' or curso_escolar like '%17')";
				$resultado = $mysqli->query($sql);
				$vacact=$resultado->num_rows;
				$pre2="<td>".$id."</td><td>".$fila['razon_social']."</td><td>".$fila['direccion']."</td><td>".$fila['codpostal']."</td><td>".$fila['poblacion']."</td><td>".$fila['provincia']."</td><td>".$fila['pais']."</td><td>".$vacact."</td>";
				$sql="SELECT * FROM vacantes WHERE idempresa = '$id'";
				$resultado = $mysqli->query($sql);
				$vacantes=$resultado->num_rows;
				if($vacantes==0){
					$dev= '<table class="table table-hover table-bordered table-condensed" >
				      <thead><tr>'.$pre1.'</tr></thead>
				      <tbody><tr>'.$pre2.'</tr></tbody></table>';
				}
				else{
					$pre3='<th colspan=3 style="text-align:center;">Histórico de vacantes</th>
							<tr><th>ID</th>
			              <th>Requisitos</th>
			              <th>Curso escolar</th></tr>';
					$pre4="";
					$sql="SELECT * FROM vacantes WHERE idempresa = '$id' ORDER BY curso_escolar DESC";
					$resultado = $mysqli->query($sql);
					while($filab = $resultado -> fetch_assoc()) {
						$pre4.="<tr><td>".$filab['id']."</td><td>".$filab['requisitostec']."</td><td>".$filab['curso_escolar']."</td></tr>";
					}
					$dev= '<table class="table table-hover table-bordered table-condensed" >
				      <thead><tr>'.$pre1.'</tr></thead>
				      <tbody><tr>'.$pre2.'</tr></tbody></table>
				      <table class="table table-hover table-bordered table-condensed" >
				      <thead><tr>'.$pre3.'</tr></thead>
				      <tbody>'.$pre4.'</tbody></table>';
				}
				
				echo $dev;

				desconectar($mysqli);
			}
			break;
	}
?>
<?php
require_once ("pintar/pintar.php");

session_start();

$titulo="Alumnos";
$var="alumno";

pinta_titulo_tabla($titulo);
pinta_tabla_alum();
pinta_modal_alum();
pinta_modal_detalle_alum();
pinta_modal_importar_alum();
pintascript($var);
<?php
session_start();
$opc=$_POST['opc'];
require_once('comprobar.php');
switch ($opc) {
	/*añadir alumno*/
	case '1':
		require_once('clasespoo/Alumno.php');
		$mail=$_POST['email'];
		$nombre=$_POST['nombre'];
		$apellidos=$_POST['apellidos'];
		$dni=$_POST['dni'];
		$fechanac=$_POST['fechanac'];
		$titulo=$_POST['titulo'];
		$direccion=$_POST['direccion'];
		$codpostal=$_POST['codpostal'];
		$poblacion=$_POST['poblacion'];
		$provincia=$_POST['provincia'];
		if($_SESSION['tipousuario']==2){
			$tutor=$_SESSION['id'];
		}
		else{
			$tutor=$_POST['tutor'];
		}
		$estudiosant=$_POST['estudiosant'];
		$experiencia=$_POST['experiencia'];
		$preferencias=$_POST['preferencias'];
		$telefono=$_POST['telefono'];
		$log="";
		if(comprmail($mail)){	
			if(comprnombre($nombre)){
				if(comprapell($apellidos)) {
                    if (comprdni($dni)) {
                        if (comprfecha($fechanac)) {
                            if (comprtitulo($titulo)) {
                                if (comprdir($direccion)) {
                                    if (comprcp($codpostal)) {
                                        if (comprpobl($poblacion)) {
                                            if (comprprov($provincia)) {
                                                if (compridtutor($tutor)) {
                                                    if (comprestant($estudiosant)) {
                                                        if (comprexp($experiencia)) {
                                                            if (comprpref($preferencias)) {
                                                                if(comprtelefono($telefono)){
                                                                    $alumno = new Alumno($mail, $nombre, $apellidos, $dni, $fechanac, $direccion, $codpostal, $poblacion, $provincia, $titulo, $estudiosant, $experiencia, $preferencias, $tutor, $telefono);
                                                                    $log = $alumno->crear($log);
                                                                }else{
                                                                    $log .= $telefono . " telefono incorrecto\r\n";
                                                                }
                                                            } else {
                                                                $log .= "el tamaño de preferencias supera el limite\r\n";
                                                            }
                                                        } else {
                                                            $log .= "el tamaño de experiencia supera el limite\r\n";
                                                        }
                                                    } else {
                                                        $log .= "el tamaño de estudios anteriores supera el limite\r\n";
                                                    }
                                                } else {
                                                    $log .= "el id tutor no es correcto\r\n";
                                                }
                                            } else {
                                                $log .= "el tamaño de provincia supera el limite\r\n";
                                            }
                                        } else {
                                            $log .= "el tamaño de poblacion supera el limite\r\n";
                                        }
                                    } else {
                                        $log .= "el codigo postal no es correcto\r\n";
                                    }
                                } else {
                                    $log .= "el tamaño de direccion supera el limite\r\n";
                                }
                            } else {
                                        $log .= "el tamaño de titulo supera el limite\r\n";
                            }
                        } else {
                            $log .= "la fecha de nacimiento no es correcto\r\n";
                        }
                    } else {
                        $log .= "el dni no es correcto\r\n";
                    }
                }else{
					$log.= "el tamaño de apellidos supera el limite\r\n";
				}
			}
			else{
				$log.= "el tamaño de nombre supera el limite\r\n";
			}
		}
		else{
			$log.= "el mail no es correcto\r\n";
		}
		echo $log;
		break;
	/*rellenar formu para editar*/
	case '2':
		require_once('BaseDatos.php');
		$id=$_POST['id'];
		$mysqli=conectar();


		$sql="SELECT * FROM usuarios INNER JOIN alumnos ON usuarios.id=alumnos.id WHERE usuarios.id='$id'";
		$result=$mysqli->query($sql);
		if($mysqli->errno){
			die('Esto va mal' .$mysqli->error);
		}
		
		$registro =$result->fetch_assoc();
		
		$retorno['nombre']=$registro['nombre'];
		$retorno['apellidos']=$registro['apellidos'];
		$retorno['email']=$registro['email'];
		$retorno['dni']=$registro['dni'];
		$retorno['fechanac']=$registro['fechanac'];
		$retorno['titulo']=$registro['titulo'];
		$retorno['direccion']=$registro['direccion'];
		$retorno['codpostal']=$registro['codpostal'];
		$retorno['poblacion']=$registro['poblacion'];
		$retorno['provincia']=$registro['provincia'];
		$retorno['telefono']=$registro['telefono'];
		$retorno['estudiosant']=$registro['estudiosant'];
		$retorno['experiencia']=$registro['experiencia'];
		$retorno['preferencias']=$registro['preferencias'];
		
		$idtutor=$registro['idtutor'];

		$sql="SELECT * FROM docentes WHERE id='$idtutor'";
		
		$result=$mysqli->query($sql);
		if($mysqli->errno){
			die('Esto va mal' .$mysqli->error);
		}
		$registro =$result->fetch_assoc();
		$tutor=$registro['nombre']." ".$registro['apellidos'];

		$retorno['tutor']=$tutor;	

		desconectar($mysqli);

		echo (json_encode($retorno));
		break;
	/*editar alumno*/
	case '3':
		require_once('clasespoo/Alumno.php');
	    $id=$_POST['clave'];
		$mail=$_POST['email'];
		$nombre=$_POST['nombre'];
		$apellidos=$_POST['apellidos'];
		$dni=$_POST['dni'];
		$fechanac=$_POST['fechanac'];
		$direccion=$_POST['direccion'];
		$codpostal=$_POST['codpostal'];
		$poblacion=$_POST['poblacion'];
		$provincia=$_POST['provincia'];
		$titulo=$_POST['titulo'];
		$estudiosant=$_POST['estudiosant'];
		$experiencia=$_POST['experiencia'];
		$preferencias=$_POST['preferencias'];
		$tutor=$_POST['tutor'];
		$telefono=$_POST['telefono'];
		$log="";

		if(comprmail($mail)){
			if(comprnombre($nombre)){
				if(comprapell($apellidos)) {
                    if (comprdni($dni)) {
	                    if (comprfecha($fechanac)) {
	                        if (comprtitulo($titulo)) {
	                            if (comprdir($direccion)) {
	                                if (comprcp($codpostal)) {
	                                    if (comprpobl($poblacion)) {
	                                        if (comprprov($provincia)) {
	                                            if (compridtutor($tutor)) {
	                                                if (comprestant($estudiosant)) {
	                                                    if (comprexp($experiencia)) {
	                                                        if (comprpref($preferencias)) {
	                                                            if(comprtelefono($telefono)){
										                            $alumno = new Alumno($mail, $nombre, $apellidos, $dni, $fechanac, $direccion, $codpostal, $poblacion, $provincia, $titulo, $estudiosant, $experiencia, $preferencias, $tutor, $telefono);
										                            $log = $alumno->modificar($id,$log);
										                        }else{
										                            $log .= $telefono . " telefono incorrecto\r\n";
										                        }
	                                                        } else {
	                                                            $log .= "el tamaño de preferencias supera el limite\r\n";
	                                                        }
	                                                    } else {
	                                                        $log .= "el tamaño de experiencia supera el limite\r\n";
	                                                    }
	                                                } else {
	                                                    $log .= "el tamaño de estudios anteriores supera el limite\r\n";
	                                                }
	                                            } else {
	                                                $log .= "el id tutor no es correcto\r\n";
	                                            }
	                                        } else {
	                                            $log .= "el tamaño de provincia supera el limite\r\n";
	                                        }
	                                    } else {
	                                        $log .= "el tamaño de poblacion supera el limite\r\n";
	                                    }
	                                } else {
	                                    $log .= "el codigo postal no es correcto\r\n";
	                                }
	                            } else {
	                                $log .= "el tamaño de direccion supera el limite\r\n";
	                            }
	                        } else {
	                            $log .= "el tamaño de titulo supera el limite\r\n";
	                        }
	                    } else {
	                        $log .= "la fecha de nacimiento no es correcto\r\n";
	                    }
	                } else {
	                    $log .= "el dni no es correcto\r\n";
	                }            
                }else{
					$log.= "el tamaño de apellidos supera el limite\r\n";
				}
			}
			else{
				$log.= "el tamaño de nombre supera el limite\r\n";
			}
		}
		else{
			$log.= "el mail no es correcto\r\n";
		}
		echo $log;

		break;
	/*eliminar alumno*/
	case '4':
		require_once('clasespoo/Usuario.php');
		$log="";
		$id=$_POST['clave'];
		$email="";

		$usuario=new Usuario($email);

		$log=$usuario->eliminar($id,$log);
		echo $log;
		break;
	/*detalle alumno*/
	case '5':
		require_once('BaseDatos.php');
		
		if( isset($_POST['id']) ){
			$mysqli=conectar();
			$id=$_POST['id'];
			$sql="SELECT * FROM usuarios WHERE id = '$id'";
			$resultado = $mysqli->query($sql);
			$fila = $resultado->fetch_assoc();
			$email=$fila['email'];
			$usuario=$fila['usuario'];
			$alta=$fila['alta'];
			$baja=$fila['baja'];

			if($fila['activo']==1){
				$pre0="<th>ID</th>
		              <th>Usuario</th>
		              <th>Fecha de alta</th>";
		        $pre1="<td>".$id."</td><td>".$usuario."</td><td>".$alta."</td>";
			}
			else{
				$pre0="<th>ID</th>
		              <th>Usuario</th>
		              <th>Fecha de alta</th>
		              <th>Fecha de baja</th>";
		        $pre1="<td>".$id."</td><td>".$usuario."</td><td>".$alta."</td><td>".$baja."</td>";	
			}
			$pre2="<th>Correo electronico</th>
	              <th>Telefono</th>
	              <th>Direccion</th>
	              <th>Codigo postal</th>
	              <th>Poblacion</th>
	              <th>Provincia</th>";
			$sql="SELECT * FROM alumnos INNER JOIN docentes ON idtutor=docentes.id WHERE alumnos.id = '$id'";
			$resultado = $mysqli->query($sql);
			$fila = $resultado->fetch_assoc();
			$pre3="<td>".$email."</td><td>".$fila['telefono']."</td><td>".$fila['direccion']."</td><td>".$fila['codpostal']."</td><td>".$fila['poblacion']."</td><td>".$fila['provincia']."</td>";
			$pre4="<th>Estudios Anteriores</th>
	              <th>Experiencia</th>
	              <th>Preferencias</th>
	              <th>Tutor</th>";
	        $id=$fila['idtutor'];
	        $sql="SELECT * FROM docentes WHERE id = '$id'";
			$resultado = $mysqli->query($sql);
			$filab = $resultado->fetch_assoc();
			$tutor=$filab['nombre']." ".$filab['apellidos'];
	        $pre5="<td>".$fila['estudiosant']."</td><td>".$fila['experiencia']."</td><td>".$fila['preferencias']."</td><td>".$tutor."</td>";
			
	  		$dev= '<table class="table table-hover table-bordered table-condensed" >
			      <thead><tr>'.$pre0.'</tr></thead>
			      <tbody><tr>'.$pre1.'</tr></tbody></table>
			      <table class="table table-hover table-bordered table-condensed" >
			      <thead><tr>'.$pre2.'</tr></thead>
			      <tbody><tr>'.$pre3.'</tr></tbody></table>
			      <table class="table table-hover table-bordered table-condensed" >
			      <thead><tr>'.$pre4.'</tr></thead>
			      <tbody><tr>'.$pre5.'</tr></tbody></table>';
			
			echo $dev;
			desconectar($mysqli);
		}
		break;
	/*importar csv alumno*/
	case '6':
		require_once('BaseDatos.php');
		require_once('clasespoo/Alumno.php');

		$log="";
		$nombre = $_FILES['fichero']['name'];
		$nombre_tmp = $_FILES['fichero']['tmp_name'];
		$tipo = $_FILES['fichero']['type'];
		$tamano = $_FILES['fichero']['size'];

		$ext_permitidas = array('csv');
		$partes_nombre = explode('.', $nombre);
		$extension = end($partes_nombre);

		//Comprobamos si la extensión es correcta
		$ext_correcta = in_array($extension, $ext_permitidas);
		//Comprobamos si el tipo es correcto (en función de los navegadores, será un tipo u otro)
		$tipos_posibles_csv = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv','application/octet-stream');
		$tipo_correcto = in_array($_FILES['fichero']['type'],$tipos_posibles_csv);
		$limite = 2 * 1024; //Máximo 2MB
		if($ext_correcta && $tipo_correcto && $tamano <= $limite){
			if($_FILES['fichero']['error'] > 0){ //Error en el fichero
				$log.= 'Error: ' . $_FILES['fichero']['error'] . '<br/>';
			}			
		} 
		else {
			$log.='Archivo inválido<br/>';
		}

		$fichero = fopen($nombre_tmp, "r");
		//alumno plantilla que se modificara en cada vuelta
		$alumno=new Alumno(null,null,null);
		$tipo=$alumno->comprueba_tipo();
		//inicializo objeto mysqli y objetos para consultas preparadas
		$mysqli=conectar();
		$stmtu = $mysqli->stmt_init();
		$stmta = $mysqli->stmt_init();
		//inicio la transaccion
		$mysqli->autocommit(false);
		//declarar las consultas a usuario y a alumnos
		$sqlu="INSERT INTO `usuarios` (`email`, `usuario`, `passwd`, `tipousuario`) VALUES (?,?,?,4)";
		$sqla="INSERT INTO `alumnos` (`id`, `nombre`, `apellidos`, `dni`, `fechanac`, `direccion`, `codpostal`, `poblacion`, `provincia`, `titulo`, `estudiosant`, `experiencia`, `preferencias`, `idtutor`, `telefono`) VALUES (?, ?, ?, ?,?, ?,?, ?, ?, ?,?, ?,?,?,?)";
		if($stmtu->prepare($sqlu)){
			if($stmta->prepare($sqla)){
				$stmtu->bind_param("sss",$email,$usuario,$hash);
				$stmta->bind_param("issssssssssssss",$id,$nombre,$apellidos,$dni,$fechanac,$direccion,$codpostal,$poblacion,$provincia,$titulo,$estudiosant,$experiencia,$preferencias,$tutor,$telefono);
				while (!feof($fichero)) {
					$linea = fgets($fichero);
					if (!empty($linea)) {
						//echo "Línea: $linea<br/>";
						$matriz = explode(",", $linea);
						$nombre = $matriz[0];
						$alumno->nombre=$nombre;
						$apellidos = $matriz[1];
						$alumno->apellidos=$apellidos;
						$email = $matriz[2];
						$alumno->email=$email;
						$alumno->regenera();
						$usuario=$alumno->usuario;
						$passwd=$alumno->passwd;
						$hash=password_hash($passwd,PASSWORD_DEFAULT);
						$dni=$matriz[3];
						$fechanac=$matriz[4];
						$direccion=$matriz[5];
						$codpostal=$matriz[6];
						$poblacion=$matriz[7];
						$provincia=$matriz[8];
						$titulo=$matriz[9];
						$estudiosant=$matriz[10];
						$experiencia=$matriz[11];
						$preferencias=$matriz[12];
						$telefono=$matriz[13];
						if($_SESSION['tipousuario']==2){
							$tutor=$_SESSION['id'];
						}
						else{
							$tutor=$matriz[14];
						}

						if(!$alumno->existe()){
							if ($stmtu->execute()){
								$id=$mysqli->insert_id;
								if ($stmta->execute()){
									$mysqli->commit();
									$para=$email;
									$cuerpo = "Bienvenido a GestFCT, tu usuario es: ".$usuario.", y tu contraseña es: ".$passwd.". Gracias por registrarte";
									$asunto = "Registro en GestFCT";

									if (mail($para, $asunto, $cuerpo)) {
										$log.= "Correo enviado con éxito</br>";
									} else {
										$log.= "Error en el envío<br/>";
									}
								}
								else{
									$log.= "fallo 1 ".$stmta->error."<br/>";
									$mysqli->rollback();
								}
							}
							else{
								$log.= "fallo 2 ".$stmtu->error."<br/>";
								$mysqli->rollback();
							}	
						}
						else{
							$log.= "el usuario ya existe<br/>";
						}			
					}
				}
				fclose($fichero);
			}
			else{
				$log.= "fallo 3<br/>";
			}
		}
		else{
			$log.= "fallo 4<br/>";
		}	
		$stmtu->close();
		$stmta->close();
		desconectar($mysqli);
		echo $log;
		break;
}
?>
<?php
require_once('comprobar.php');
$opc=$_POST['opc'];
session_start();
switch ($opc) {
	/*añadir fct*/
	case '1':
		require_once('funciones.php');
		require_once('clasespoo/Fct.php');
		$alumno=$_POST['alumno'];
		if($_SESSION['tipousuario']==1){
			$mysqli=conectar();
			$sql="SELECT idtutor FROM alumnos inner join usuarios on alumnos.id=usuarios.id where activo=1";
			$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}
			$fila=$resultado->fetch_assoc();
			$docente=$fila['idtutor'];
			desconectar($mysqli);
		}
		else if($_SESSION['tipousuario']==2){
			$docente=$_SESSION['id'];
		}
		$fecha=new DateTime ($_POST['inicio']);
		$inicio=new DateTime ($_POST['inicio']);
		$inicio=(string)$inicio->format('Y-m-d');
		$vacante=$_POST['vacante'];
		$log="";
		if(compridalum($alumno)){
			if(compridvac($vacante)){
				if(comprfecha($inicio)){
					$horas=total_horas($alumno);
					$fin=fechafin($inicio,$horas);
					$fct=new Fct($alumno,$docente,$vacante,$inicio,$fin,$horas);
					$log=$fct->crear($log);
				}
				else{
					$log.="fecha inicio no valida\r\n";
				}
			}
			else{
				$log.="id vacante no valido\r\n";
			}
		}
		else{
			$log.="id alumno no valido\r\n";
		}

		echo $log;
		break;
	/*mostrar detalle fct*/
	case '2':
		require_once ("pintar/pintar.php");
		if($_SESSION['tipousuario']==4){
			require_once ("BaseDatos.php");
			$alumno=$_SESSION['id'];
			$mysqli=conectar();
			$sql="SELECT id FROM fct where alumno=$alumno";
			$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}
			$fila=$resultado->fetch_assoc();
			$idfct=$fila['id'];
			desconectar($mysqli);
			$_SESSION['idfct']=$idfct;
		}
		elseif($_SESSION['tipousuario']==1||$_SESSION['tipousuario']==2){
			$_SESSION['idfct']=$_POST['id'];
		}
		$var="fcth";
		pinta_tabla_fcthoras();
		pinta_modal_fcth();
		pintascript($var);
		break;
	/*insertar fct horas diario*/
	case '3':
		require_once('clasespoo/FctHoras.php');
		require_once('funciones.php');
		$log="";
		$fct=$_SESSION['idfct'];
		$mysqli=conectar();
		$sql="SELECT inicio, fin from fct where id=$fct";
		$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}
		$fila=$resultado->fetch_assoc();
		$inicio=$fila['inicio'];
		$fin=$fila['fin'];
		desconectar($mysqli);
		$fecha=$_POST['fecha'];
		$numhoras=$_POST['numhoras'];
		$observaciones=$_POST['observaciones'];
		if(comprfechad($fecha,$inicio,$fin)){
			if(comprnumhoras($numhoras)){
				if(comprobs($observaciones)){	
					$fcth=new FctHoras($fct,$fecha,$numhoras,$observaciones);
					$log=$fcth->crear($log);
					$log=actualizar_fin($fct,$log);
				}
				else{
					$log.="observaciones incorrecto\r\n";
				}
			}else{
				$log.="numero de horas incorrecto\r\n";
			}
		}else{
			$log.="fecha no valida\r\n";
		}
		echo $log;
		break;
	/*modificar fct horas diario*/
	case '4':
		require_once('clasespoo/FctHoras.php');
		require_once('funciones.php');
		$fecha=$_POST['fecha'];
		$numhoras=$_POST['numhoras'];
		$observaciones=$_POST['observaciones'];
		$fct=$_SESSION['idfct'];
		$mysqli=conectar();
		$sql="SELECT inicio, fin from fct where id=$fct";
		$resultado=$mysqli->query($sql);
			if($mysqli->errno){
				die('Esto va mal' .$mysqli->error);
			}
		$fila=$resultado->fetch_assoc();
		$inicio=$fila['inicio'];
		$fin=$fila['fin'];
		desconectar($mysqli);
		$id=$_POST['clave'];
		$log="";
		if(comprfechad($fecha,$inicio,$fin)){
			if(comprnumhoras($numhoras)){
				if(comprobs($observaciones)){	
					$fcth=new FctHoras($fct,$fecha,$numhoras,$observaciones);
					$log=$fcth->modificar($id,$log);
					$log=actualizar_fin($fct,$log);
				}
				else{
					$log.="observaciones incorrecto\r\n";
				}
			}else{
				$log.="numero de horas incorrecto\r\n";
			}
		}else{
			$log.="fecha no valida\r\n";
		}
		echo $log;
		break;
}
?>
<?php
function pinta_tabla_fct(){
echo '
		<div class="container">
			<div class="display responsive no-wrap">
				<table id="tabla" class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th data-priority="1">Alumno</th>
							<th >Docente</th>
							<th >Inicio</th>
							<th >Fin</th>
							<th >Horas realizadas</th>
							<th >Vacante</th>
							<th>ID</th>
							<th ></th>
						</tr>
					</thead>
				</table>
			</div><!--Fin class="display responsive no-wrap"-->
';
}

function pinta_tabla_fcthoras(){
echo '
			<div class="display responsive no-wrap">
				<table id="fcth" class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th data-priority="1" width="20%">Fecha</th>
							<th width="20%">Horas realizadas</th>
							<th width="50%">Observaciones</th>
							<th>ID</th>
							<th>FCT</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div><!--Fin class="display responsive no-wrap"-->
';
}

function pinta_modal_fct(){
echo '
			<div class="modal fade" id="miVentana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Titulo</h4>
						</div>
						<div class="modal-body">
							<form id=miFormulario>
								<div class="form-group hidden">
									<label for="clave">ID</label>
									<input type="text" name="clave" dissabled id="clave">
								</div>
								
								<div class="form-group row">
									<label class="col-xs-4" for="alumno">Alumno</label>
									<select class="col-xs-7" name="alumno" id="alumno">';
								if($_SESSION['tipousuario']==1){
									opciona();
								}
								elseif($_SESSION['tipousuario']==2){
									opcionat();
								}
echo										
									'</select>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="inicio">Inicio</label>
									<input type="text" class="col-xs-7" name="inicio" id="inicio" placeholder="Inicio" required>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="vacante">Vacante</label>
									<select class="col-xs-7" name="vacante" id="vacante">';
									opcionv();

echo										
									'</select>
								</div>

								<button type="submit" class="btn btn-default btn-primary" id="btnSubmit">Submit</button>
							</form>
						</div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pinta_modal_fcth(){
echo '
			<div class="modal fade" id="ventfcth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="tituloh"><strong>Añadir registro diario</strong></h4>
						</div>
						<div class="modal-body">
							<form id=formfcth>
								<div class="form-group hidden">
									<label for="clave">ID</label>
									<input type="text" name="clave" dissabled id="clave">
								</div>

								<div class="form-group hidden">
									<label for="clavefct">FCT</label>
									<input type="text" name="clavefct" dissabled id="clavefct">
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="fecha">Fecha</label>
									<input type="text" class="col-xs-7" name="fecha" id="fecha" placeholder="Fecha" required>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="numhoras">Horas realizadas</label>
									<input type="number" class="col-xs-7" name="numhoras" id="numhoras" placeholder="Horas realizadas" min="0" max="10" required>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="observaciones">Observaciones</label>
									<textarea class="col-xs-7" name="observaciones" id="observaciones" placeholder="Observaciones" rows="3" cols="47" maxlength="60" required></textarea>
								</div>

								<button type="submit" class="btn btn-primary" id="btnSubmith">Guarda nuevo</button>
							</form>
						</div>
						<div class="modal-footer">
							<button id="cerrarfcth" type="button" class="btn btn-default"><!--data-dismiss="modal"-->Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pinta_modal_detalle_fct(){
echo '
			<div class="modal fade bs-example-modal-lg" id="ventanaDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Detalle de usuario</h4>
						</div>
						<div class="modal-body" id="tabladetalle"></div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}
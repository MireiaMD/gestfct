<?php

function pintabody_cuerpo_login(){
echo '
			<div id="cuerpo">
				<div class="row">

					<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-primary">

							<div class="panel-heading center-block">
								<div class="panel-title">
								<h3>Identificación</h3>
								</div>
							</div><!--Fin class="panel-heading center-block"-->

							<div class="panel-body" STYLE=" margin-right:10px; margin-left:10px;">
							<form class="form-horizontal" action="login.php" id="ident" method="POST">';
}

function pinta_alerta_login($alerta){
if(!isset($_POST['enviar'])){
echo '
								<div class="hide alert alert-danger" role="alert" id="alerta">Alguno de los campos está vacio</div>';
}
else{
echo '
								<div class="alert alert-danger" role="alert" id="alerta" align="center">'.$alerta.'</div>';
}
}

function pinta_formulario_login(){
echo '
								<div class="form-group">
									<div class="col-xs-12" >
										<label for="usuario">Usuario</label>
									</div>
									<div class="col-xs-12 input-group">
										<span class="input-group-addon" id="basic-addon1">
										<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
										</span>
										<input type="text" class="form-control" id="usuario" placeholder="Usuario" name="usuario" aria-describedby="basic-addon1">
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12">
										<label for="passwd">Contraseña</label>
									</div>
									<div class="col-xs-12 input-group">
										<span class="input-group-addon" id="basic-addon2">
										<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
										</span>
										<input type="password" class="form-control" id="passwd" placeholder="Contraseña" name="passwd" aria-describedby="basic-addon2">
									</div>
								</div>

								<input type="submit" class="btn btn-primary center-block" id="enviar" name="enviar" value="Enviar">
							</form>
						</div><!--Fin class="panel-body"-->

						<div class="panel-footer">Introduce usuario y contraseña para identificarte</div>

					</div><!--Fin class="panel panel-primary"-->
				</div><!-- Fin class="col-md-6 col-md-offset-3"-->
			</div><!--Fin class="row"-->
		</div><!--Fin id="corpo"-->';
}

function pintajquery_login(){
echo '
		<!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
		<script src="../js/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/login.js"></script>
';
}
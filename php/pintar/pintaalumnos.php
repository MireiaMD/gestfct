<?php
function pinta_tabla_alum(){
echo '
		<div class="container">
			<div class="display responsive no-wrap">
				<table id="tabla" class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th data-priority="1" width="17%">Nombre</th>
							<th width="20%">Apellidos</th>
							<th width="15%">DNI</th>
							<th width="15%">Fecha de Nacimiento</th>
							<th width="18%">Titulo</th>
							<th>ID</th>
							<th>IDtutor</th>
							<th width="15%"></th>
						</tr>
					</thead>
				</table>
			</div><!--Fin class="display responsive no-wrap"-->
';
}

function pinta_modal_alum(){
echo '
			<div class="modal fade" id="miVentana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Titulo</h4>
						</div>
						<div class="modal-body">
							<form id=miFormulario>
								<div class="form-group hidden">
									<label for="clave">ID</label>
									<input type="text" name="clave" dissabled id="clave">
								</div>
								
								<div class="form-group row">
									<label class="col-xs-4" for="nombre">Nombre</label>
									<input type="text" class="col-xs-7" name="nombre" id="nombre" placeholder="Nombre" required>
								</div>
								<div class="form-group row">
									<label class="col-xs-4" for="apellidos">Apellidos</label>
									<input type="text" class="col-xs-7" name="apellidos" id="apellidos" placeholder="Apellidos" required>
								</div>
								
								<div class="form-group row">
									<label class="col-xs-4" for="dni">DNI</label>
									<input type="text" class="col-xs-7" name="dni" id="dni" placeholder="DNI">
								</div>

								<div class="form-group row" id="grupomail">
									<label class="col-xs-4" for="email">Correo electronico</label>
									<input type="email" class="col-xs-7" name="email" id="email" placeholder="Correo electronico" required>
								</div>

								<div class="form-group row" id="grupoalta">
									<label class="col-xs-4" for="alta">Fecha de alta</label>
									<input type="text" class="col-xs-7" name="alta" id="alta" placeholder="Fecha alta" disabled>
								</div>
								
								<div class="form-group row hidden" id="grupobaja">
									<label class="col-xs-4" for="baja">Fecha de baja</label>
									<input type="text" class="col-xs-7" name="baja" id="baja" placeholder="Fecha baja" disabled>
								</div>
								
								<div id="altaeditar">
									<div class="form-group row">
										<label class="col-xs-4" for="fechanac">Fecha de nacimiento</label>
										<input type="text" class="col-xs-7" name="fechanac" id="fechanac" placeholder="Fecha de nacimiento">
									</div>

									<div class="form-group row">
										<label class="col-xs-4" for="direccion">Direccion</label>
										<input type="text" class="col-xs-7" name="direccion" id="direccion" placeholder="Direccion">
									</div>

									<div class="form-group row">
										<label class="col-xs-4" for="codpostal">Codigo postal</label>
										<input type="text" class="col-xs-7" name="codpostal" id="codpostal" placeholder="Codigo postal">
									</div>
									 
									<div class="form-group row">
										<label class="col-xs-4" for="poblacion">Poblacion</label>
										<input type="text" class="col-xs-7" name="poblacion" id="poblacion" placeholder="Poblacion">
									</div>

									<div class="form-group row">
										<label class="col-xs-4" for="provincia">Provincia</label>
										<input type="text" class="col-xs-7" name="provincia" id="provincia" placeholder="Provincia">
									</div>

									<div class="form-group row">
										<label class="col-xs-4" for="telefono">Telefono</label>
										<input type="text" class="col-xs-7" name="telefono" id="telefono" placeholder="Telefono" required>
									</div>

									<div class="form-group row">
										<label class="col-xs-4" for="titulo">Titulo</label>
										<input type="text" class="col-xs-7" name="titulo" id="titulo" placeholder="Titulo">
									</div>

									<div class="form-group row" id="grupotutorA">
										<label class="col-xs-4" for="tutorA">Tutor</label>
										<select class="col-xs-7" name="tutor" id="tutorA">';
opciont();

echo										
										'</select>
									</div>
									<div class="form-group row" id="grupotutorB">
									</div>
								</div>

';

echo '
								<div class="hidden" id="alumnoe">
									<div class="form-group row">
										<label class="col-xs-4" for="estudiosant">Estudios anteriores</label>
										<input type="text" class="col-xs-7" name="estudiosant" id="estudiosant" placeholder="Estudios anteriores">
									</div>

									<div class="form-group row">
										<label class="col-xs-4" for="experiencia">Experiencia</label>
										<textarea class="col-xs-7" name="experiencia" id="experiencia" placeholder="Experiencia" rows="3" cols="47" maxlength="60"></textarea>
									</div>

									<div class="form-group row">
										<label class="col-xs-4" for="preferencias">Preferencias</label>
										<textarea class="col-xs-7" name="preferencias" id="preferencias" placeholder="Preferencias" rows="3" cols="47" maxlength="60"></textarea>
									</div>
								</div>
';

echo '
								<button type="submit" class="btn btn-default btn-primary" id="btnSubmit">Submit</button>
							</form>
						</div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pinta_modal_detalle_alum(){
echo '
			<div class="modal fade bs-example-modal-lg" id="ventanaDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Detalle de usuario</h4>
						</div>
						<div class="modal-body hidden" id="tablaalumno"></div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pinta_modal_importar_alum(){
echo '
			<div class="modal fade" id="ventanaImportar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Importar alumnos</h4>
						</div>
						<div class="modal-body">
							<form enctype="multipart/form-data" method="POST" id="subir">
								<!-- MAX_FILE_SIZE debe preceder al campo de entrada del fichero -->
								<input type="hidden" name="MAX_FILE_SIZE" value="1000" required="" />
								<div class="form-group row">
									<label class="col-xs-4" for="fichero">Selecciona fichero CSV</label>
									<input class="col-xs-7" type="file" name="fichero" id="fichero"/>
								</div>

								<div class="form-group row hidden">
									<label class="col-xs-4" for="opc">opc</label>
									<input class="col-xs-7" type="text" name="opc" id="opc" value="6"/>
								</div>
									
								<button type="submit" class="btn btn-default btn-primary" id="enviar">Enviar fichero</button>
							</form>
							<div id="mensaje"></div>
						</div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pintascript_alum(){
	echo '
		<script src="../js/alumno.js"></script>
';
}



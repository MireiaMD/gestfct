<?php

function pinta_tabla_tutor(){
echo '
		<div class="container">
			<div class="display responsive no-wrap">
				<table id="tabla" class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th data-priority="1">Nombre</th>
							<th>Apellidos</th>
							<th>DNI</th>
							<th>ID</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div><!--Fin class="display responsive no-wrap"-->
';
}

function pinta_modal_tutor(){
echo '
			<div class="modal fade" id="miVentana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Titulo</h4>
						</div>
						<div class="modal-body">
							<form id=miFormulario>
								<div class="form-group hidden">
									<label for="clave">ID</label>
									<input type="text" name="clave" dissabled id="clave">
								</div>';
echo '
								<div class="form-group row">
									<label class="col-xs-4" for="nombre">Nombre</label>
									<input type="text" class="col-xs-7" name="nombre" id="nombre" placeholder="Nombre" required>
								</div>
								<div class="form-group row">
									<label class="col-xs-4" for="apellidos">Apellidos</label>
									<input type="text" class="col-xs-7" name="apellidos" id="apellidos" placeholder="Apellidos" required>
								</div>
								<div class="form-group row">
									<label class="col-xs-4" for="dni">DNI</label>
									<input type="text" class="col-xs-7" name="dni" id="dni" placeholder="DNI">
								</div>

								<div class="form-group row" id="grupomail">
									<label class="col-xs-4" for="email">Correo electronico</label>
									<input type="email" class="col-xs-7" name="email" id="email" placeholder="Correo electronico" required>
								</div>

								<div class="form-group row" id="grupoalta">
									<label class="col-xs-4" for="alta">Fecha de alta</label>
									<input type="text" class="col-xs-7" name="alta" id="alta" placeholder="Fecha alta" disabled>
								</div>
								<div class="form-group row hidden" id="grupobaja">
									<label class="col-xs-4" for="baja">Fecha de baja</label>
									<input type="text" class="col-xs-7" name="baja" id="baja" placeholder="Fecha baja" disabled>
								</div>
								<div class="form-group row hidden" id="grupousuario">
									<label class="col-xs-4" for="usuario">Usuario</label>
									<input type="text" class="col-xs-7" name="usuario" id="usuario" placeholder="Usuario">
								</div>
';

echo '
								<button type="submit" class="btn btn-default btn-primary" id="btnSubmit">Submit</button>
							</form>
						</div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pinta_modal_detalle_tutor(){
echo '
			<div class="modal fade bs-example-modal-lg" id="ventanaDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Detalle de usuario</h4>
						</div>
						<div class="modal-body" id="tabladetalle"></div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default btn-danger" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pintascript_tutor(){
	echo '
		<script src="../js/tutor.js"></script>
';
}



<?php

function pintainicio(){
echo '
<!DOCTYPE html> 
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../img/favicon.ico">

		<title>GestFCT</title>

		<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../datatables/datatables.min.css" rel="stylesheet">
		<link href="../css/bootstrap-datepicker3.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../dist/sweetalert.css">
		<link href="../css/miestilo.css" rel="stylesheet">
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

		<!-- Custom styles for this template -->


		<!-- Just for debugging purposes. Dont actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<script src="../datatables/datatables.min.js"></script>
		<script src="../js/bootstrap-datepicker.min.js"></script>
		<script src="../js/bootstrap-datepicker.es.min.js"></script>
		<script src="../dist/sweetalert.min.js"></script>
		<script src="../js/botones.js"></script>
	</head>
';
}

function pinta_navbar_completa($navbar,$link){
echo '
		<header>
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<img class="img-responsive" src="../img/logo mireia.png" alt="logotipo" width="30">
				</div><!--Fin navbar-header-->
				<div id="navbar" class="navbar-collapse collapse">
				'.$navbar.'
					<ul class="nav navbar-nav navbar-right">'.$link.'
						<li><a class="milink" onclick="perfil()">Mi perfil</a></li>
						<li class="active"><a onclick="cerrar();">Cerrar Sesion</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</nav>
		</header>';
}

function pintabody_encabezado(){
echo '
	<body>
		<div class="container" style="padding-top:15px">';
}

function pinta_navbar_base(){
echo '
		<header>
			<nav class="navbar navbar-default">
			<div class="row">
				<div class="col-md-3 col-md-offset-1" ><img class="img-rounded" img-responsive" src="../img/logo_mireia_final.png" alt="logotipo" width="120"></div>
				<div class="col-md-8"><h1>Sistema de gestion de FCT &#39; s</h1></div>
			</div><!--Fin row-->
			</nav>
		</header>';
}

function pinta_titulo_tabla($titulo){
echo '
	<div class="container">
	<h1 class="text-center datatitulo">'.$titulo.'</h1>
	</div>';
}

function pintascript($var){
	echo '
		<script src="../js/'.$var.'.js"></script>
';
}

function pintafin(){
echo "
		</div><!--Fin container-->
	</body>
</html>
";
}
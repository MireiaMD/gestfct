<?php
function pinta_tabla_emp(){
echo '
		<div class="container">
			<div class="display responsive no-wrap">
				<table id="tabla" class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th data-priority="1" width="15%">Nombre</th>
							<th width="15%">Tutor</th>
							<th width="15%">Telefono</th>
							<th width="15%">Móvil</th>
							<th width="20%">Correo electronico</th>
							<th>ID</th>
							<th width="15%"></th>
						</tr>
					</thead>
				</table>
			</div><!--Fin class="display responsive no-wrap"-->
';
}

function pinta_modal_emp(){
echo '
			<div class="modal fade" id="miVentana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Titulo</h4>
						</div>
						<div class="modal-body">
							<form id=miFormulario>
								<div class="form-group hidden">
									<label for="clave">ID</label>
									<input type="text" name="clave" dissabled id="clave">
								</div>
								
								<div class="form-group row">
									<label class="col-xs-4" for="nombre">Nombre</label>
									<input type="text" class="col-xs-7" name="nombre" id="nombre" placeholder="Nombre" required>
								</div>
								<div class="form-group row">
									<label class="col-xs-4" for="razon_social">Razon social</label>
									<input type="text" class="col-xs-7" name="razon_social" id="razon_social" placeholder="Razon social">
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="direccion">Direccion</label>
									<input type="text" class="col-xs-7" name="direccion" id="direccion" placeholder="Direccion">
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="codpostal">Codigo postal</label>
									<input type="text" class="col-xs-7" name="codpostal" id="codpostal" placeholder="Codigo postal">
								</div>
								 
								<div class="form-group row">
									<label class="col-xs-4" for="poblacion">Poblacion</label>
									<input type="text" class="col-xs-7" name="poblacion" id="poblacion" placeholder="Poblacion">
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="provincia">Provincia</label>
									<input type="text" class="col-xs-7" name="provincia" id="provincia" placeholder="Provincia">
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="pais">Pais</label>
									<input type="text" class="col-xs-7" name="pais" id="pais" placeholder="Pais">
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="tutorempresa">Tutor empresa</label>
									<input type="text" class="col-xs-7" name="tutorempresa" id="tutorempresa" placeholder="Tutor empresa" required>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="email">Correo electronico</label>
									<input type="email" class="col-xs-7" name="email" id="email" placeholder="Correo electronico" required>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="telefono">Telefono</label>
									<input type="text" class="col-xs-7" name="telefono" id="telefono" placeholder="Telefono" required>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="movil">Movil</label>
									<input type="text" class="col-xs-7" name="movil" id="movil" placeholder="Movil" required>
								</div>
								
								<button type="submit" class="btn btn-default btn-primary" id="btnSubmit">Submit</button>
							</form>
						</div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pinta_modal_detalle_emp(){
echo '
			<div class="modal fade bs-example-modal-lg" id="ventanaDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Detalle de usuario</h4>
						</div>
						<div class="modal-body" id="tablaempresa"></div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pinta_modal_altavac(){
echo '
			<div class="modal fade" id="ventanaAnadirvac" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title titulo" id="myModalLabel">Añadir Vacante</h4>
						</div>
						<div class="modal-body">
							<form id=formVacante>
								<div class="form-group hidden">
									<label for="clave">ID</label>
									<input type="text" name="clave" dissabled id="clave">
								</div>
								
								<div class="form-group row hidden">
									<label class="col-xs-4" for="idempresa">idempresa</label>
									<input type="text" class="col-xs-7" name="idempresa" dissabled id="idempresa" placeholder="idempresa">
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="requisitostec">Requisitos tecnicos</label>
									<textarea class="col-xs-7" name="requisitostec" id="requisitostec" placeholder="Requisitos tecnicos" rows="3" cols="47" maxlength="60" required></textarea>
								</div>

								<div class="form-group row">
									<label class="col-xs-4" for="curso_escolar">A&ntilde;o</label>
									<input type="data" class="col-xs-7" name="curso_escolar" id="curso_escolar" placeholder="A&ntilde;o" required>
								</div>
								
								<button type="submit" class="btn btn-primary" id="guardar">Guardar</button>
							</form>
						</div>
						<div class="modal-footer">
							<button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
';
}

function pintascript_emp(){
	echo '
		<script src="../js/empresa.js"></script>
';
}



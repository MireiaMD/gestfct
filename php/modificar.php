<?php
require_once ("BaseDatos.php");
session_start();
if(isset($_POST['op'])){
	switch ($_POST['op']) {
		case 'datosp':
			if($_SESSION['tipousuario']==4){
				$mysqli=conectar();
				$id=$_SESSION['id'];
				$sql="SELECT * from alumnos inner join usuarios on usuarios.id=alumnos.id where usuarios.id=$id";
				$resultado = $mysqli->query($sql);
				$fila = $resultado->fetch_assoc();
				$email=$fila['email'];
				$nombre=$fila['nombre'];
				$apellidos=$fila['apellidos'];
				$dni=$fila['dni'];
				$telefono=$fila['telefono'];
				$direccion=$fila['direccion'];
				$codpostal=$fila['codpostal'];
				$poblacion=$fila['poblacion'];
				$provincia=$fila['provincia'];
				$idtutor=$fila['idtutor'];
				$titulo=$fila['titulo'];
				$fechanac=$fila['fechanac'];
				$estudiosant=$fila['estudiosant'];
				$preferencias=$fila['preferencias'];
				$experiencia=$fila['experiencia'];
				desconectar($mysqli);
				$formu='
				<form id="miFormulario">
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group hidden">
								<label for="tipo">tipo</label>
								<input type="text" class="form-control" name="tipo" id="tipo" value="datos">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="nombre">Nombre</label>
								<input class="col-xs-7" type="text" class="form-control" name="nombre" id="nombre" value="'.$nombre.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="apellidos">Apellidos</label>
								<input class="col-xs-7" type="text" class="form-control" name="apellidos" id="apellidos" value="'.$apellidos.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="dni">DNI</label>
								<input class="col-xs-7" type="text" class="form-control" name="dni" id="dni" value="'.$dni.'">
							</div>
							
							<div class="form-group row">
								<label class="col-xs-4" for="fechanac">Fecha de nacimiento</label>
								<input class="col-xs-7" type="text" class="form-control" name="fechanac" id="fechanac" value="'.$fechanac.'">
							</div>
							
							<div class="form-group row">
								<label class="col-xs-4" for="titulo">Titulo</label>
								<input class="col-xs-7" type="text" class="form-control" name="titulo" id="titulo" value="'.$titulo.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="estudiosant">Estudios Anteriores</label>
								<input class="col-xs-7" type="text" class="form-control" name="estudiosant" id="estudiosant" value="'.$estudiosant.'">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group row">
								<label class="col-xs-4" for="email">Correo electronico</label>
								<input class="col-xs-7" type="email" class="form-control" name="email" id="email" value="'.$email.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="telefono">Telefono</label>
								<input class="col-xs-7" type="text" class="form-control" name="telefono" id="telefono" value="'.$telefono.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="direccion">Direccion</label>
								<input class="col-xs-7" type="text" class="form-control" name="direccion" id="direccion" value="'.$direccion.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="codigopostal">Codigo postal</label>
								<input class="col-xs-7" type="text" class="form-control" name="codpostal" id="copostal" value="'.$codpostal.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="poblacion">Poblacion</label>
								<input class="col-xs-7" type="text" class="form-control" name="poblacion" id="poblacion" value="'.$poblacion.'">
							</div>

							<div class="form-group row">
								<label class="col-xs-4" for="provincia">Provincia</label>
								<input class="col-xs-7" type="text" class="form-control" name="provincia" id="provincia" value="'.$provincia.'">
							</div>
							<div class="form-group row hidden">
								<label class="col-xs-4" for="idtutor">idtutor</label>
								<input class="col-xs-7" type="text" class="form-control" name="idtutor" id="idtutor" value="'.$idtutor.'">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group row col-xs-12">
							<label class="col-xs-4" for="experiencia">Experiencia</label>
							<textarea class="col-xs-8" name="experiencia" id="experiencia" rows="3" cols="47" maxlength="60">'.$experiencia.'</textarea>
						</div>
					</div>
					<div class="row">
						<div class="form-group row col-xs-12">
							<label class="col-xs-4" for="preferencias">Preferencias</label>
							<textarea class="col-xs-8" name="preferencias" id="preferencias" rows="3" cols="47" maxlength="60">'.$preferencias.'</textarea>
						</div>
					</div>
				</form>
				<script>
				$("#fechanac").datepicker({
			        format: "yyyy-mm-dd",
			        startDate: "1947-01-01",
			        endDate: "2002-01-01",
			        startView: 1,
			        maxViewMode: 2,
			        language: "es",
			        autoclose: true  
			    });
				</script>';
			}
			else if($_SESSION['tipousuario']==1){
				$mysqli=conectar();
				$id=$_SESSION['id'];
				$sql="SELECT email,nombre,apellidos from administradores inner join usuarios on usuarios.id=administradores.id where usuarios.id=$id";
				$resultado = $mysqli->query($sql);
				$fila = $resultado->fetch_assoc();
				$email=$fila['email'];
				$nombre=$fila['nombre'];
				$apellidos=$fila['apellidos'];
				desconectar($mysqli);
				$formu='<form action="" method="get" id="miFormulario">
				<div class="form-group hidden">
				<label for="tipo">tipo</label>
				<input type="text" class="form-control" name="tipo" id="tipo" value="datos">
				</div>
				
				<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" name="nombre" id="nombre" value="'.$nombre.'">
				</div>

				<div class="form-group">
				<label for="apellidos">Apellidos</label>
				<input type="text" class="form-control" name="apellidos" id="apellidos" value="'.$apellidos.'">
				</div>

				<div class="form-group">
				<label for="email">Correo electronico</label>
				<input type="email" class="form-control" name="email" id="email" value="'.$email.'">
				</div>';
			}
			else {
				$mysqli=conectar();
				$id=$_SESSION['id'];
				$sql="SELECT email,nombre,apellidos,dni from docentes inner join usuarios on usuarios.id=docentes.id where usuarios.id=$id";
				$resultado = $mysqli->query($sql);
				$fila = $resultado->fetch_assoc();
				$email=$fila['email'];
				$nombre=$fila['nombre'];
				$apellidos=$fila['apellidos'];
				$dni=$fila['dni'];
				desconectar($mysqli);
				$formu='<form action="" method="get" id="miFormulario">
				<div class="form-group hidden">
				<label for="tipo">tipo</label>
				<input type="text" class="form-control" name="tipo" id="tipo" value="datos">
				</div>

				<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" name="nombre" id="nombre" value="'.$nombre.'">
				</div>

				<div class="form-group">
				<label for="apellidos">Apellidos</label>
				<input type="text" class="form-control" name="apellidos" id="apellidos" value="'.$apellidos.'">
				</div>

				<div class="form-group">
				<label for="email">Correo electronico</label>
				<input type="email" class="form-control" name="email" id="email" value="'.$email.'">
				</div>

				<div class="form-group">
				<label for="dni">DNI</label>
				<input type="text" class="form-control" name="dni" id="dni" value="'.$dni.'">
				</div>

				</form>';	
			}
			$mtitulo='<h4 class="modal-title" id="myModalLabel">Modificacion datos personales</h4>';
		break;
		case 'contra':
			$mtitulo='<h4 class="modal-title" id="myModalLabel">Modificacion contraseña</h4>';
			$formu='<form action="" method="get" id="miFormulario">
			<div class="form-group hidden">
			<label for="tipo">tipo</label>
			<input type="text" class="form-control" name="tipo" id="tipo" value="pass">
			</div>

			<div class="form-group">
			<label for="passwda">Contraseña actual</label>
			<input type="password" class="form-control" name="passwda" id="passwda" placeholder="Contraseña actual">
			</div>

			<div class="form-group">
			<label for="passwdb">Contraseña nueva</label>
			<input type="password" class="form-control" name="passwdb" id="passwdb" placeholder="Contraseña nueva">
			</div>

			<div class="form-group">
			<label for="passwdc">Repita contraseña nueva</label>
			<input type="password" class="form-control" name="passwdc" id="passwdc" placeholder="Contraseña nueva">
			</div>
			</form>';
		break;
	}
	$modal='<div class="modal-dialog" role="document" id="ventModif">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$mtitulo.'</div>
	  <div class="modal-body">'.$formu.'</div>
	  <div class="modal-footer">
	    <input type="button" class="btn btn-primary" id="guardar" name="guardar" value="Guardar">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	  </div>
	</div>
	</div>';
	echo $modal;
}
if(isset($_POST['tipo'])){
	if($_POST['tipo']=="datos"){
		require_once('comprobar.php');
		$id=$_SESSION['id'];
	    $tipousu=$_SESSION['tipousuario'];
	    $log="";
		switch ($tipousu) {
			case 1:
				$tabla="administradores";
				$email=obtener($tabla,"email",$id);
				$nombre=obtener($tabla,"nombre",$id);
				$apellidos=obtener($tabla,"apellidos",$id);
				if(comprmail($email)){
					if(comprnombre($nombre)){
						if(comprapell($apellidos)){
							require_once ('clasespoo/Admin.php');
							$administrador=new Administrador($email,$nombre,$apellidos);
							$log.=$administrador->modificar($id,$log);
						}else{$log.="Apellidos demasiado largo\r\n";}
					}else{$log.="Nombre demasiado largo\r\n";}
				}else{$log.="Correo electronico incorrecto\r\n";}
				echo $log;				
				break;
			case 2:
				$tabla="docentes";
				$email=obtener($tabla,"email",$id);
				$nombre=obtener($tabla,"nombre",$id);
				$apellidos=obtener($tabla,"apellidos",$id);
				$dni=obtener($tabla,"dni",$id);
				if(comprmail($email)){
					if(comprnombre($nombre)){
						if(comprapell($apellidos)){
							if(comprdni($dni)){
								require_once ('clasespoo/idtutor.php');
								$tutor=new Tutor($email,$nombre,$apellidos,$dni);
								$log.=$tutor->modificar($id,$log);
							}else{$log.="Dni demasiado largo\r\n";}
						}else{$log.="Apellidos demasiado largo\r\n";}
					}else{$log.="Nombre demasiado largo\r\n";}
				}else{$log.="Correo electronico incorrecto\r\n";}
				echo $log;		
				break;
			case 3:
				$tabla="docentes";
				$email=obtener($tabla,"email",$id);
				$nombre=obtener($tabla,"nombre",$id);
				$apellidos=obtener($tabla,"apellidos",$id);
				$dni=obtener($tabla,"dni",$id);
				if(comprmail($email)){
					if(comprnombre($nombre)){
						if(comprapell($apellidos)){
							if(comprdni($dni)){
								require_once ('clasespoo/idtutor.php');
								$profesor=new Profesor($email,$nombre,$apellidos,$dni);
								$log.=$profesor->modificar($id,$log);
							}else{$log.="Dni demasiado largo\r\n";}
						}else{$log.="Apellidos demasiado largo\r\n";}
					}else{$log.="Nombre demasiado largo\r\n";}
				}else{$log.="Correo electronico incorrecto\r\n";}
				echo $log;
				break;
			case 4:
				$tabla="alumnos";
				$email=obtener($tabla,"email",$id);
				$nombre=obtener($tabla,"nombre",$id);
				$apellidos=obtener($tabla,"apellidos",$id);
				$dni=obtener($tabla,"dni",$id);				
				$fechanac=obtener($tabla,"fechanac",$id);
				$direccion=obtener($tabla,"direccion",$id);
				$codpostal=obtener($tabla,"codpostal",$id);
				$poblacion=obtener($tabla,"poblacion",$id);
				$provincia=obtener($tabla,"provincia",$id);
				$titulo=obtener($tabla,"titulo",$id);
				$estudiosant=obtener($tabla,"estudiosant",$id);
				$experiencia=obtener($tabla,"experiencia",$id);
				$preferencias=obtener($tabla,"preferencias",$id);
				$idtutor=obtener($tabla,"idtutor",$id);
				$telefono=obtener($tabla,"telefono",$id);
				if(comprmail($email)){
					if(comprnombre($nombre)){
						if(comprapell($apellidos)){
							if(comprdni($dni)){
								if(comprfecha($fechanac)){
									if(comprdir($direccion)){
										if(comprcp($codpostal)){
											if(comprpobl($poblacion)){
												if(comprprov($provincia)){
													if(comprtitulo($titulo)){
														if(comprestant($estudiosant)){
															if(comprexp($experiencia)){
																if(comprpref($preferencias)){
																	if(compridtutor($idtutor)){
																		if(comprtelefono($telefono)){
																			require_once ('clasespoo/Alumno.php');
																			$alumno=new Alumno($email,$nombre,$apellidos,$dni,$fechanac,$direccion,$codpostal,$poblacion,$provincia,$titulo,$estudiosant,$experiencia,$preferencias,$idtutor,$telefono);
																			$log.=$alumno->modificar($id,$log);
																		}else{$log.="Telefono incorrecto\r\n";}
																	}else{$log.="Id tutor invalido\r\n";}
																}else{$log.="Preferencias demasiado largo\r\n";}
															}else{$log.="Experiencia demasiado largo\r\n";}
														}else{$log.="Estudios anteriores demasiado largo\r\n";}
													}else{$log.="Titulo demasiado largo\r\n";}
												}else{$log.="Provincia demasiado larga\r\n";}
											}else{$log.="Poblacion demasiado larga\r\n";}
										}else{$log.="Codigo postal incorrecto\r\n";}
									}else{$log.="Direccion demasiado larga\r\n";}
								}else{$log.="Fecha incorrecta\r\n";}
							}else{$log.="Dni demasiado largo\r\n";}
						}else{$log.="Apellidos demasiado largo\r\n";}
					}else{$log.="Nombre demasiado largo\r\n";}
				}else{$log.="Correo electronico incorrecto\r\n";}
				echo $log;
				break;
		}
	}
	else if($_POST['tipo']=="pass"){
		$id=$_SESSION['id'];
		$mysqli=conectar();
		$sql="SELECT passwd from usuarios where id='$id'";
		$resultado = $mysqli->query($sql);
		$fila = $resultado->fetch_assoc();
		$hash=$fila['passwd'];
		$passwd=$_POST['passwda'];
		$log="";
		if(password_verify($passwd, $hash)){
			$npasswd=$_POST['passwdb'];
			$nrpasswd=$_POST['passwdc'];
			if($npasswd==$nrpasswd){
				$hash=password_hash($npasswd,PASSWORD_DEFAULT);
				$hash=$mysqli->real_escape_string($hash);
				$sql = "UPDATE usuarios SET passwd = '".$hash."' where id=$id";
				
				$mysqli->query($sql);  
				if($mysqli->connect_errno){
					$log.="Error en consulta\r\n";
				}

				$log.= "Registro guardado\r\n";
			}
			else{
				$log.= "no se puede modificar, los campos nueva contraseña no coinciden.\r\n";
			}
		}
		else{
			$log.= "no se puede modificar, contraseña incorrecta.\r\n";
		}
	desconectar($mysqli);
	echo $log;	
	}
}
function obtener($tabla,$campo,$id){
	if($_POST[$campo]==""){
		$mysqli=conectar();
		$sql="SELECT * from $tabla inner join usuarios on usuarios.id=$tabla.id where usuarios.id='$id'";
		$resultado = $mysqli->query($sql);
		$fila = $resultado->fetch_assoc();
		$valor=$fila[$campo];
		desconectar($mysqli);
	}
	else{
		$valor=$_POST[$campo];
	}
	return $valor;
}
?>
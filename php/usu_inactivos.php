<?php
require_once ("pintar/pintar.php");

session_start();

$titulo="Usuarios inactivos";
$navbar='
					<ul class="nav navbar-nav">
						<p style="padding-top:15px; padding-left:30px; font-size:120%; font-weight: bold">Usuarios Inactivos</p>
					</ul><!--Fin class="nav navbar-nav"-->
';
$var="usuinact";

pinta_titulo_tabla($titulo);
pinta_tabla_base_inactivos();
pinta_modal_detalle();
pintascript($var);
<?php
require_once ("pintar/pintar.php");

session_start();


if($_SESSION['tipousuario']==1){
	$cargar='
		<div class="row" id="fechahora">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-primary" id="pfechahora">
			  		<div class="panel-body">
			  			<span id="liveclock"></span>
			  		</div>
	  			</div>
	  		</div>
	  	</div>';
	$link='<li><a class="milink" href="principal.php">Inicio</a></li>';
	$navbar=
		'<ul class="nav navbar-nav">
		<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios <span class="caret"></span></a>
		<ul class="dropdown-menu">
		<li><a onclick="usuact();">Usuarios activos</a></li>
		<li><a onclick="usuinact();">Usuarios inactivos</a></li>
		</ul>
		</li>
		<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Docentes <span class="caret"></span></a>
		<ul class="dropdown-menu">
		<li><a onclick="tutores();">Tutores</a></li>
		<li><a onclick="profesores();">Profesores</a></li>
		</ul>
		</li>
		<li><a onclick="alumnos();">Alumnos</a></li>
		<li><a onclick="empresas();">Empresas</a></li>
		<li><a onclick="fcts();">FCT&#039;s</a></li>
		</ul>';

	pintainicio();
	pintabody_encabezado();
	pinta_navbar_completa($navbar,$link);
	pintabody_cuerpo_usuario($cargar);
	pintafin();

}
else if($_SESSION['tipousuario']==2){
	$cargar='
		<div class="row" id="fechahora">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-primary" id="pfechahora">
			  		<div class="panel-body">
			  			<span id="liveclock"></span>
			  		</div>
	  			</div>
	  		</div>
	  	</div>';
	$link='<li><a class="milink" href="principal.php">Inicio</a></li>';
	$navbar=
		'<ul class="nav navbar-nav">
		<li><a onclick="alumnos();">Alumnos</a></li>
		<li><a onclick="empresas();">Empresas</a></li>
		<li><a onclick="fcts();">FCT&#039;s</a></li>
		</ul>';

	pintainicio();
	pintabody_encabezado();
	pinta_navbar_completa($navbar,$link);
	pintabody_cuerpo_usuario($cargar);
	pintafin();
}
/*else if($_SESSION['tipousuario']==3){

}*/
else if($_SESSION['tipousuario']==4){
	$usuario=$_SESSION['usuario'];
	$alumno="";
	$mysqli=conectar();
	
	$sql="SELECT `alumnos`.`nombre` as nomalumno,`alumnos`.`apellidos` as apellalumno,`docentes`.`nombre` as nomdocente,`docentes`.`apellidos` as apelldocente,inicio,fin,horas,vacante FROM fct inner join usuarios on alumno=usuarios.id inner join alumnos on alumno=alumnos.id inner join docentes on docente=docentes.id where usuarios.usuario='$usuario'";
	$resultado=$mysqli->query($sql);
	if($mysqli->errno){
		die('Esto va mal' .$mysqli->error);
	}
	$fila=$resultado->fetch_assoc();
	$inicio=$fila['inicio'];
	$fin=$fila['fin'];
	$horas=$fila['horas'];
	$idvac=$fila['vacante'];
	$alumno=$fila['nomalumno']." ".$fila['apellalumno'];
	$docente=$fila['nomdocente']." ".$fila['apelldocente'];
	$navbar='<span id="bienvenido">Bienvenido '.$alumno.'</span>';
	$link='<li><a class="milink" href="principal.php">Mi fct</a></li>';
	
	$sql="SELECT `nombre`,`tutorempresa`,`telefono`,`movil`,`email` FROM vacantes inner join empresas on empresas.id=idempresa where vacantes.id=$idvac";
	$resultadob=$mysqli->query($sql);
	if($mysqli->errno){
		die('Esto va mal' .$mysqli->error);
	}
	$filab=$resultadob->fetch_assoc();
	$empresa=$filab['nombre'];
	$tutorempresa=$filab['tutorempresa'];
	$telefono=$filab['telefono'];
	$movil=$filab['movil'];
	$email=$filab['email'];
	$panel='
		<div class="panel panel-primary">
	  		<div class="panel-heading">
				<div class="panel-title">
					<h3>Detalle de mi FCT</h3>
				</div>
	  		</div>
	  		<div class="panel-body">
				<div class="row">
					<div class="col-xs-3">
						<p><span class="etiqueta">Fecha de inicio:&nbsp;</span>'.$inicio.'</p>
					</div>
					<div class="col-xs-3">
						<p><span class="etiqueta">Fecha de fin:&nbsp;</span>'.$fin.'</p>
					</div>
					<div class="col-xs-2">
						<p><span class="etiqueta">Horas realizadas:&nbsp;</span>'.$horas.'</p>
					</div>
					<div class="col-xs-4">
						<p><span class="etiqueta">Tutor:&nbsp;</span>'.$docente.'</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3">
						<p><span class="etiqueta">Empresa:&nbsp;</span>'.$empresa.'</p>
					</div>
					<div class="col-xs-4">
						<p><span class="etiqueta">Tutor en empresa:&nbsp;</span>'.$tutorempresa.'</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-2">
						<p><span class="etiqueta">Teléfono:&nbsp;</span>'.$telefono.'</p>
					</div>
					<div class="col-xs-2">
						<p><span class="etiqueta">Móvil:&nbsp;</span>'.$movil.'</p>
					</div>
					<div class="col-xs-4">
						<p><span class="etiqueta">Correo electrónico:&nbsp;</span>'.$email.'</p>
					</div>
				</div>
				<div class="row" id="diario" style="padding: 20px 10px;">
					
				</div>
	  		</div>
	  		<div class="panel-footer">
				<button type="submit" class="btn btn-primary btn-sm" id="btndiario" name="btndiario" value="ver">Ver diario</button>
	  		</div>
		</div>';
		$var="mifct";
	desconectar($mysqli);
	pintainicio();
	pintabody_encabezado();
	pinta_navbar_completa($navbar,$link);
	pintabody_cuerpo_usuario($panel);
	pintascript($var);
	pintafin();
}
else{
	header("Location: ../portada.php");
}

?>
<?php
require_once ("pintar/pintar.php");

session_start();

$alerta="";
if(empty($_SESSION['usuario'])){
	
	if(isset($_POST['enviar'])){
		
		if (!empty($_POST['usuario'])&&!empty($_POST['passwd'])){

			$mysqli = new mysqli('localhost','mireia','renaido','proyecto');   
			
			if($mysqli->connect_errno){   
				$alerta="Error de conexión";
			}

			else {
				$usuario=$mysqli->real_escape_string($_POST['usuario']);
				$passwd=$mysqli->real_escape_string($_POST['passwd']);
			
				$sql="SELECT id,usuario,passwd,tipousuario,activo from usuarios where usuario='$usuario'";
				$resultado = $mysqli->query($sql);
				$fila = $resultado->fetch_assoc();
				$id=$fila['id'];
				$usuario=$fila['usuario'];
				$hash=$fila['passwd'];
				$tipo=$fila['tipousuario'];
				$activo=$fila['activo'];

				if($activo==1 && password_verify($passwd, $hash)){
					$_SESSION['id']=$id;
					$_SESSION['usuario']=$usuario;
					$_SESSION['tipousuario']=$tipo;
					header ('Location: principal.php');
				}
				else{
					$alerta= "Alguno de los datos no es correcto";
				}
			}
			$mysqli->close();
		}
		else{
			$alerta= "Faltan campos por cubrir";
		}
	}
}

	pintainicio();
	pintabody_encabezado();
	pinta_navbar_base();
	pintabody_cuerpo_login();
	pinta_alerta_login($alerta);
	pinta_formulario_login();
	pintafin();



<?php
	require_once('BaseDatos.php');
	//session_start();
	function opciont(){
		$mysqli=conectar();
		$opciones="";
		$sql="SELECT * FROM docentes INNER JOIN usuarios ON usuarios.id=docentes.id WHERE tipousuario=2 and activo=1";
		$resultado = $mysqli->query($sql);
		$bandera=false;
			while($fila = $resultado->fetch_assoc()) {  
				if(!$bandera){
					$opciones.= "<option value='".$fila['id']."' selected>".$fila['nombre']." ".$fila['apellidos']."</option>"; 
					$bandera=true;
				}
				elseif($bandera){
					$opciones.= "<option value='".$fila['id']."'>".$fila['nombre']." ".$fila['apellidos']."</option>"; 
				}
			}
		desconectar($mysqli);
		echo $opciones;
	}



	function opciona(){
		$mysqli=conectar();
		$opciones="";
		$sql="SELECT alumnos.id,nombre,apellidos,idtutor FROM alumnos INNER JOIN usuarios ON usuarios.id=alumnos.id WHERE activo=1 and alumnos.id not in (SELECT alumno FROM fct)";
		$resultado = $mysqli->query($sql);
		$bandera=false;
			while($fila = $resultado->fetch_assoc()) {  
				if(!$bandera){
					$opciones.= "<option value='".$fila['id']."' selected>".$fila['nombre']." ".$fila['apellidos']."</option>"; 
					$bandera=true;
				}
				elseif($bandera){
					$opciones.= "<option value='".$fila['id']."'>".$fila['nombre']." ".$fila['apellidos']."</option>"; 
				}
			}
		desconectar($mysqli);
		echo $opciones;
	}

	function opcionat(){
		$id=$_SESSION['id'];
		$mysqli=conectar();
		$opciones="";
		$sql="SELECT alumnos.id,nombre,apellidos,idtutor FROM alumnos INNER JOIN usuarios ON usuarios.id=alumnos.id WHERE activo=1 and idtutor=$id and alumnos.id not in (SELECT alumno FROM fct)";
		$resultado = $mysqli->query($sql);
		$bandera=false;
			while($fila = $resultado->fetch_assoc()) {  
				if(!$bandera){
					$opciones.= "<option value='".$fila['id']."' selected>".$fila['nombre']." ".$fila['apellidos']."</option>"; 
					$bandera=true;
				}
				elseif($bandera){
					$opciones.= "<option value='".$fila['id']."'>".$fila['nombre']." ".$fila['apellidos']."</option>"; 
				}
			}
		desconectar($mysqli);
		echo $opciones;
	}

	function opcionv(){
		$mysqli=conectar();
		$opciones="";
		//$sql="SELECT vacantes.id, nombre, requisitostec FROM empresas INNER JOIN vacantes ON empresas.id=idempresa WHERE activo=1 and (curso_escolar like '%2017%' or curso_escolar like '%17%') and vacantes.id not in (SELECT vacante FROM fct)";
		$sql="SELECT vacantes.id, nombre, requisitostec FROM empresas INNER JOIN vacantes ON empresas.id=idempresa WHERE (curso_escolar like '%2017%' or curso_escolar like '%17%') and vacantes.id not in (SELECT vacante FROM fct)";
		$resultado = $mysqli->query($sql);
		$bandera=false;
			while($fila = $resultado->fetch_assoc()) {  
				if(!$bandera){
					$opciones.= "<option value='".$fila['id']."' selected>".$fila['nombre'].", REQUISITOS:".$fila['requisitostec']."</option>"; 
					$bandera=true;
				}
				elseif($bandera){
					$opciones.= "<option value='".$fila['id']."'>".$fila['nombre'].", REQUISITOS:".$fila['requisitostec']."</option>"; 
				}
			}
		desconectar($mysqli);
		echo $opciones;
	}

	function total_horas($alumno){
		$total_horas=0;
		$mysqli=conectar();
        $sql="SELECT numhoras FROM fcthoras inner join fct on fct.id=idfct where alumno=$alumno";
		$resultado=$mysqli->query($sql);
		if($mysqli->errno){
			die('Esto va mal' .$mysqli->error);
		}
		while($fila =$resultado->fetch_assoc()){
			$total_horas+=$fila['numhoras'];
		}
		return $total_horas;
		$mysqli->close();	
	}

	function fechafin($fechainicio,$horasrealizadas){
		$horastot=384;
		$horasfaltan=$horastot-$horasrealizadas;
		if ($horasfaltan<$horastot){
			$fechafin=new DateTime();
		}
		else{
			$fechafin=new DateTime($fechainicio);
		}
		$intervalo="P1D";
		while($horasfaltan>0){	
			$fechafin=$fechafin->add(new DateInterval($intervalo));
			$dia=date("w", strtotime($fechafin->format('Y-m-d')));
			if($dia!="0" && $dia!="6"){
				$horasfaltan-=8;
			}
		}
		return (string)$fechafin->format('Y-m-d');
	}

	function actualizar_fin($idfct,$log){
		$mysqli=conectar();
		$sql="SELECT alumno,inicio FROM fct WHERE id=$idfct";
		$result=$mysqli->query($sql);
		if($mysqli->errno){
			$log.=("Error en consulta");
		}
		$registro=$result->fetch_assoc();
		$alumno=$registro['alumno'];
		$inicio=$registro['inicio'];
		$horasrealizadas=total_horas($alumno);
		$fin=fechafin($inicio,$horasrealizadas);
		
		$sql = "UPDATE fct SET fin = '".$fin."', horas = '".$horasrealizadas."' where id=$idfct";
		$mysqli->query($sql);  
		if($mysqli->connect_errno){
			$log.=("Error en consulta");
		}
		$log.= "modifique fecha";
		desconectar($mysqli);
		return $log;
	}
?>
